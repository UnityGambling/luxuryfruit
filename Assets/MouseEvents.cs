﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class MouseEvents : MonoBehaviour {

	public DigitalSpriteFontDisplay MouseText = null;
	public DigitalSpriteFontDisplay MousePosX = null;
	public DigitalSpriteFontDisplay MousePosY = null;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		MousePosX.SetValue((int)Input.mousePosition.x);
		MousePosY.SetValue((int)Input.mousePosition.y);
	}

	public void OnMouseUp()
	{
		Debug.Log("OnMouseUp");
		MouseText.SetValue(0);
		
	}

	public void OnMouseDown()
	{
		Debug.Log("OnMouseDown");
		MouseText.SetValue(1);
	}

	public void OnMouseDrag()
	{
		Debug.Log("OnMouseDrag");
		MouseText.SetValue(2);
	}

	public void OnMouseEnter()
	{
		Debug.Log("OnMouseEnter");
		MouseText.SetValue(3);
	}

	public void OnMouseExit()
	{
		Debug.Log("OnMouseExit");
		MouseText.SetValue(4);
	}

}
