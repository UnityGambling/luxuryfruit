﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DigitalSpriteFontDisplay : MonoBehaviour 
{
	public DigitalSpriteFontAsset 	DigitalSpriteFont 	= null;
	public Color 					DigitColor 			= Color.white;
	public float					Kerning				= 0.05f;
	public string 					SortingLayerName 	= "Front";
	public int 						SortingOrder 		= 10;
    //public int credits = 10000000;

    public bool isUGUI = false;
    public GameObject               digitPrefab         = null;

	public void SetValue( int number )
	{
		while ( transform.childCount > 0 )
		{
			DestroyImmediate(transform.GetChild(0).gameObject);
		}

		char[] digits = Mathf.Abs( number ).ToString().ToCharArray();

        if (isUGUI)
        {
            float totalLength = 0;
            for (int i = 0; i <= digits.Length - 1; i++)
            {
                Image uiImage = AddUIImage(digitPrefab, DigitalSpriteFont.Digits[digits[i] - '0']);
                totalLength += (uiImage.sprite.bounds.size.x + (i == digits.Length - 1 ? 0 : Kerning));
            }
        }
        else
        {
            float totalLength = 0;
            for (int i = digits.Length - 1; i >= 0; --i)
            {
                GameObject spriteGo = new GameObject();
                spriteGo.transform.parent = transform;
                spriteGo.transform.localPosition = Vector3.zero;
                spriteGo.transform.localRotation = Quaternion.identity;

                SpriteRenderer spriteRenderer = spriteGo.AddComponent<SpriteRenderer>();
                spriteRenderer.color = DigitColor;
                spriteRenderer.sortingLayerName = SortingLayerName;
                spriteRenderer.sortingOrder = SortingOrder;
                spriteRenderer.sprite = DigitalSpriteFont.Digits[digits[i] - '0'];

                totalLength -= (spriteRenderer.bounds.size.x + (i == digits.Length - 1 ? 0 : Kerning));
                spriteGo.transform.Translate(totalLength, 0f, 0f);
            }
        }
	}

    private Image AddUIImage(GameObject prefab, Sprite sprite = null)
    {
        GameObject spriteGo = (GameObject)Instantiate(prefab);
        spriteGo.transform.SetParent(transform, false);
        spriteGo.transform.localPosition = Vector3.zero;
        spriteGo.transform.localRotation = Quaternion.identity;

        if(sprite != null)
        { 
            Image uiImage = spriteGo.GetComponent<Image>();
            uiImage.color = DigitColor;
            uiImage.sprite = sprite;
            return uiImage;
        }
        return null;
    }

    //void Update()
    //{
    //    SetValue(credits);
    //}
}
