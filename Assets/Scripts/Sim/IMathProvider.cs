﻿using LuxuryFruit.Library.Core.DTO;
using System;

namespace LuxuryFruit.Library.Simulator
{
    /// <summary>
    /// Abstraction for the math layer used by the Platform
    /// </summary>
    public interface IMathProvider
    {
        /// <summary>
        /// Initialize the underlying math structures.
        /// </summary>
        void Startup();
        /// <summary>
        /// Shutdown the underlying math structures
        /// </summary>
        void Shutdown();

        /// <summary>
        /// Generates an outcome based on the spin request
        /// </summary>
        /// <param name="credits">The current amount of credits before generating this outcome</param>
        /// <param name="request">The request data to generate an outcome for</param>
        /// <returns>A new OutcomeRecord from the SpinRequest</returns>
		OutcomeRecordDTO GenerateOutcome(double credits, SpinRequestDTO request);

        /// <summary>
        /// Get the reel strip configuration
        /// </summary>
        /// <returns>the reel strips - first index addresses a reel, second index is a symbol location, the value is a symbol id</returns>
        long[][] GetReelStrips();

        /// <summary>
        /// Get the payline configuration
        /// </summary>
        /// <returns>the payline configuration - first index addresses a payline, second index addresses a reel, the value is +/- offset from the reel stop</returns>
        long[][] GetPaylines();

        /// <summary>
        /// Get the max bet per line
        /// </summary>
        /// <returns>the max bet per line</returns>
		double GetMaxBetPerLine();
    }
}
