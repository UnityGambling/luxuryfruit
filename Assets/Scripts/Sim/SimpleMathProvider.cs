﻿using LuxuryFruit.Library.Core.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LuxuryFruit.Library.Simulator
{
    public class SimpleMathProvider : IMathProvider
    {
        private readonly long[][] lines = new long[][] {
            new long[] { 0, 0, 0, 0, 0 },       //payout line 0
            new long[] { 1, 1, 1, 1, 1,},       //payout line 1
            new long[] { -1, -1, -1, -1, -1 },  //payout line 2
            new long[] { -1, -1, 0, 1, 1 },     //payout line 3
            new long[] { 1, 1, 0, -1, -1 },     //payout line 4
            new long[] { 1, 0, -1, 0, 1 },      //payout line 5
            new long[] { -1, 0, 1, 0, -1 },     //payout line 6
            new long[] { 1, -1, 1, -1, 1 },      //payout line 7
            new long[] { -1, 1, -1, 1, -1 },     //payout line 8
        };

        private readonly long[][] strips = new long[][] {
            new long[] { 10, 0, 7, 1, 0, 6, 9, 2, 3, 0, 8, 1, 0, 5, 0, 1, 0, 4, 0 },    //reel strip 0
            new long[] { 0, 1, 10, 6, 3, 0, 4, 0, 4, 0, 5, 2, 8, 0, 9, 2, 0, 7, 1 },    //reel strip 1
            new long[] { 9, 7, 0, 10, 2, 0, 8, 1, 5, 0, 6, 0, 0, 4, 2, 3, 1, 3, 2 },    //reel strip 2
            new long[] { 0, 1, 7, 0, 3, 2, 0, 5, 4, 0, 1, 8, 0, 5, 9, 10, 6, 0, 2 },    //reel strip 3
            new long[] { 8, 0, 1, 2, 0, 7, 0, 10, 0, 4, 6, 0, 9, 5, 0, 1, 0, 3, 1 },    //reel strip 4
        };

        private Random random = new Random();

        public void Startup()
        {
            //No need to initialize anything for fake math here.
        }

        public void Shutdown()
        {
            //No need to shut anything down for fake math here.
        }

        /// <summary>
        /// Gets an array describing the reel strips
        /// </summary>
        /// <returns>First index is the reel, second index is the symbol id number</returns>
        public long[][] GetReelStrips()
        {
            return strips;
        }

        /// <summary>
        /// Gets an array describing the paylines
        /// </summary>
        /// <returns>First index is the payline, second index is the reel number</returns>
        public long[][] GetPaylines()
        {
            return lines;
        }

        public double GetMaxBetPerLine()
        {
            return 5;
        }

        private long[] GenerateStops()
        {
            int stripLength = strips[0].Length;
            return new long[] { random.Next(stripLength), random.Next(stripLength), random.Next(stripLength), random.Next(stripLength), random.Next(stripLength) };
        }

        private long GetSymbolId( long reel, long paylineOffset, long[] stops )
        {
            long offset = stops[reel] + paylineOffset;
            return strips[reel][ArrayMod(offset, strips[reel].Length)];
        }

        private int ArrayMod(long x, int m)
        {
            return (int)((x % m + m) % m);
        }

		private LineWinDTO[] CalculateWins(double betPerLine, long numLines, long[] stops)
        {
            List<LineWinDTO> wins = new List<LineWinDTO>();

            for (int payline = 0; payline < lines.Length && payline < numLines; ++payline )
            {
                // Look up the symbols
                long[] symbols = new long[] 
                { 
                    GetSymbolId(0, lines[payline][0], stops ),
                    GetSymbolId(1, lines[payline][0], stops ),
                    GetSymbolId(2, lines[payline][0], stops ),
                    GetSymbolId(3, lines[payline][0], stops ),
                    GetSymbolId(4, lines[payline][0], stops ),
                };

                //Do the payouts. Win amount is roughly: (symbol index) * ( rarity ) * bet

                // If all of the symbols match
                if ( symbols[0] == symbols[1] &&
                     symbols[0] == symbols[2] &&
                     symbols[0] == symbols[3] &&
                     symbols[0] == symbols[4] )
                {
                    wins.Add(new LineWinDTO { line_index = payline, symbols = new bool[] { true, true, true, true, true }, win = (symbols[0] + 1) * 2500 * betPerLine });
                }
                // If the first four symbols match
                else if ( symbols[0] == symbols[1] &&
                          symbols[0] == symbols[2] &&
                          symbols[0] == symbols[3] )
                {
                    wins.Add(new LineWinDTO { line_index = payline, symbols = new bool[] { true, true, true, true, false }, win = (symbols[0] + 1) * 500 * betPerLine });
                }
                // If the first 3 symbols match
                else if (symbols[0] == symbols[1] &&
                    symbols[0] == symbols[2] )
                {
                    wins.Add(new LineWinDTO { line_index = payline, symbols = new bool[] { true, true, true, false, false }, win = (symbols[0] + 1) * 250 * betPerLine });
                }
                // If the first 2 symbols match
                else if (symbols[0] == symbols[1] )
                {
                    wins.Add(new LineWinDTO { line_index = payline, symbols = new bool[] { true, true, false, false, false }, win = (symbols[0] + 1) * 100 * betPerLine });
                }
            }

            return wins.ToArray();
        }

		private double CalculateCreditBalance(double startCredits, BetConfigDTO bet, LineWinDTO[] wins)
        {
			double result = startCredits - (bet.bet_per_line * bet.num_lines);

            foreach ( LineWinDTO win in wins )
            {
                result += win.win;
            }

            return result;
        }

		public OutcomeRecordDTO GenerateOutcome(double credits, SpinRequestDTO request)
        {
            long[] stops = GenerateStops();
            LineWinDTO[] lineWins = CalculateWins( request.bet.bet_per_line, request.bet.num_lines, stops );

            return new OutcomeRecordDTO 
            {
                 bet = request.bet,
                 id = 0, //This will be populated by the persistence layer
                 credits = CalculateCreditBalance( credits, request.bet, lineWins ),
                 reel_stops = stops,
                 status = Library.Core.OutcomeStatus.Started,
                 wins = lineWins
            };
        }
    }
}
