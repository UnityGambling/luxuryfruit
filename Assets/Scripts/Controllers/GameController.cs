﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;
using LuxuryFruit.Library.Core.DTO;

public class GameController : MonoBehaviour {

    public static GameController instance = null;

    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
            //if not, set instance to this
            instance = this;
        //If instance already exists and it's not this:
        else if (instance != this)
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
    }
    
        // Use this for initialization
        void Start ()
    {
        // If in the Unity Editor we don't want to load the other scene while using Multi Scene Editing. Then it gets loaded twice
#if !UNITY_EDITOR
        //UnityEngine.SceneManagement.SceneManager.LoadScene("main", UnityEngine.SceneManagement.LoadSceneMode.Additive);
#endif
    }
}
