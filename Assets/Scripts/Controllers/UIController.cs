﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;
using LuxuryFruit.Library.Core.DTO;

public class UIController : MonoBehaviour {

    public static UIController instance = null;
    public DigitalSpriteFontDisplay wonMeter = null;
    public DigitalSpriteFontDisplay lineBetMeter = null;
    public DigitalSpriteFontDisplay linesMeter = null;
    public DigitalSpriteFontDisplay betMeter = null;
    public DigitalSpriteFontDisplay creditMeter = null;

    private RootPresentation rootPresentation = null;

    private double bet = 0;

    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
            //if not, set instance to this
            instance = this;
        //If instance already exists and it's not this:
        else if (instance != this)
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
    }
    
    void OnEnable()
    {
        Domain.Game.CreditsChange += OnCreditsChanged;
        Domain.Game.BetLinesChange += OnBetLinesChanged;
        Domain.Game.WinsChange += OnWinsChanged;
        Domain.Game.BetPerLineChange += OnBetPerLineChanged;
    }

    private void OnDisable()
    {
        Domain.Game.CreditsChange -= OnCreditsChanged;
        Domain.Game.BetLinesChange -= OnBetLinesChanged;
        Domain.Game.WinsChange -= OnWinsChanged;
        Domain.Game.BetPerLineChange -= OnBetPerLineChanged;
    }

    // Use this for initialization
    void Start ()
    {
        rootPresentation = RootPresentation.instance;
        Debug.Log("root presentation = " + rootPresentation);
    }

    // Update is called once per frame
    void Update () {
	
	}

    /// <summary>
    /// Called from the OnClick() method of the spin button
    /// </summary>
    public void Spin()
    {
        rootPresentation.Panel.OnSpinButtonClick();
    }

    public void MaxBet()
    {
        rootPresentation.Panel.OnBetMaxButtonClick();
    }

    public void BetPlus()
    {
        if (!Domain.Game.AllowInput) { return; }

        if (Domain.Game.BetPerLine >= Domain.Machine.MaxBetPerLine) { return; }

        AdjustBet(Domain.Game.BetPerLine + 1, Domain.Game.BetLines);
    }

    public void BetMinus()
    {
        if (!Domain.Game.AllowInput) { return; }

        if (Domain.Game.BetPerLine <= 1) { return; }

        AdjustBet(Domain.Game.BetPerLine - 1, Domain.Game.BetLines);
    }

    public void LinesPlus()
    {
        if (!Domain.Game.AllowInput) { return; }

        int maxLines = Domain.Machine.PaylineDefinitions.Length;

        if (Domain.Game.BetLines >= maxLines) { return; }

        AdjustBet(Domain.Game.BetPerLine, Domain.Game.BetLines + 1);
    }

    public void LinesMinus()
    {
        if (!Domain.Game.AllowInput) { return; }

        if (Domain.Game.BetLines <= 1) { return; }

        AdjustBet(Domain.Game.BetPerLine, Domain.Game.BetLines - 1);
    }

    private void OnCreditsChanged()
    {
        Debug.Log("credits updated: " + Domain.Game.Credits);
        creditMeter.SetValue((int)Domain.Game.Credits);
    }
    private void OnBetLinesChanged()
    {
        rootPresentation.Panel.OnBetLinesChanged();
        creditMeter.SetValue((int)Domain.Game.Credits);

        bet = Domain.Game.BetPerLine * Domain.Game.BetLines;
        linesMeter.SetValue((int)Domain.Game.BetLines);
        betMeter.SetValue((int)bet);
    }

    private void OnBetPerLineChanged()
    {
        bet = Domain.Game.BetPerLine * Domain.Game.BetLines;
        lineBetMeter.SetValue((int)Domain.Game.BetPerLine);
        betMeter.SetValue((int)bet);
    }

    private void OnWinsChanged()
    {
        double total = 0;
        foreach (LineWinDTO win in Domain.Game.Wins)
        {
            total += win.win;
        }
        wonMeter.SetValue((int)total);
    }


    public void AdjustBet(double targetBetPerLine, long targetNumLines)
    {
        double betDelta = targetBetPerLine - Domain.Game.BetPerLine;
        long lineDelta = targetNumLines - Domain.Game.BetLines;

        double newBet = (Domain.Game.BetPerLine + betDelta) * (Domain.Game.BetLines + lineDelta);

        if (newBet > Domain.Game.Credits) { return; }

        bet = newBet;
        Domain.Game.BetPerLine = Domain.Game.BetPerLine + betDelta;
        Domain.Game.BetLines = Domain.Game.BetLines + lineDelta;
        betMeter.SetValue((int)bet);
    }

}
