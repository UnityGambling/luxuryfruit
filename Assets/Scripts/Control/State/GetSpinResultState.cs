﻿using UnityEngine;
using LuxuryFruit.Library.Client;
using LuxuryFruit.Library.Core;
using LuxuryFruit.Library.Core.DTO;

public class GetSpinResultState : StateBase 
{
	private IClientCommProvider comm = null;
	private ReelSetPresentation reels = null;
	
	private CountDownLatch latch;
	private OutcomeStatus status;

	private void OnEnable()
	{
		latch = new CountDownLatch(2);
		latch.Synchronize += OnSynchronize;

		comm = (IClientCommProvider)gameObject.GetComponent(typeof(IClientCommProvider));
		comm.ResultResponse += OnResultResponse;
		comm.ResultRequest(new ResultRequestDTO { id = Domain.Game.SpinID } );

		reels = GameObject.Find("root").GetComponent<RootPresentation>().Reels;
		reels.SpinPresentationComplete += OnSpinPresentationComplete;
		if (!reels.Spinning)
		{
			OnSpinPresentationComplete();
		}
	}

	private void OnSynchronize()
	{
		if ( status != OutcomeStatus.Completed )
		{
			Transition("CompleteSpinState");
		}
		else
		{
			Transition("WaitForInputState");
		}
	}
	
	private void OnSpinPresentationComplete()
	{
		latch.Signal();
	}

	private void OnResultResponse( ResultResponseDTO response )
	{
		Domain.Game.Credits 	= response.outcome.credits;
		Domain.Game.BetPerLine 	= response.outcome.bet.bet_per_line < 1 ? 1 : response.outcome.bet.bet_per_line;
		Domain.Game.BetLines 	= response.outcome.bet.num_lines < 1 ? 1 : response.outcome.bet.num_lines;
		Domain.Game.ReelStops 	= response.outcome.reel_stops;
		Domain.Game.Wins 		= response.outcome.wins;
		Domain.Game.SpinID		= response.outcome.id;

		status = response.outcome.status;

		latch.Signal();
	}

	private void OnDisable()
	{
		latch.Synchronize -= OnSynchronize;

		reels.SpinPresentationComplete -= OnSpinPresentationComplete;
		comm.ResultResponse -= OnResultResponse;
		comm = null;
	}
}
