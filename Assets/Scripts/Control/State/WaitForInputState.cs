﻿using UnityEngine;

public class WaitForInputState : StateBase 
{
	private PanelPresentation panel = null;

	private void OnEnable()
	{
		Domain.Game.AllowInput = true;

		panel = GameObject.Find("root").GetComponent<RootPresentation>().Panel;
		panel.Spin += OnSpin;
	}

	private void OnSpin()
	{
		Transition("SpinState");
	}

	private void OnDisable()
	{
		panel.Spin -= OnSpin;
		Domain.Game.AllowInput = false;
	}
}
