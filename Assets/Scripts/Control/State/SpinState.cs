﻿using UnityEngine;
using LuxuryFruit.Library.Client;
using LuxuryFruit.Library.Core;
using LuxuryFruit.Library.Core.DTO;

public class SpinState : StateBase 
{
	private IClientCommProvider comm = null;

	private void OnEnable()
	{
		comm = (IClientCommProvider)gameObject.GetComponent(typeof(IClientCommProvider));
		comm.SpinResponse += OnSpinResponse;
		comm.SpinRequest(new SpinRequestDTO
		{
			bet = new BetConfigDTO 
			{ 
				bet_per_line = Domain.Game.BetPerLine,
				num_lines = Domain.Game.BetLines,
			}
		});
	}

	private void OnSpinResponse( SpinResponseDTO response )
	{
		//if (!response.successful)
		//{
		//	Debug.LogWarning("Platform returned an unsuccessful spin request.");
		//	Transition("WaitForInputState");
		//	return;
		//}

		Domain.Game.BetPerLine  = response.bet.bet_per_line < 1 ? 1 : response.bet.bet_per_line;
		Domain.Game.BetLines    = response.bet.num_lines < 1 ? 1 : response.bet.num_lines;
		Domain.Game.Credits     = response.credits;
		Domain.Game.SpinID      = response.id;

		Transition("GetSpinResultState");
	}

	private void OnDisable()
	{
		comm.SpinResponse -= OnSpinResponse;
		comm = null;
	}
}
