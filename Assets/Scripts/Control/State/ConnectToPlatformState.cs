﻿using UnityEngine;
using LuxuryFruit.Library.Client;

public class ConnectToPlatformState : StateBase 
{
	private IClientCommProvider comm = null;

	private void OnEnable()
	{
		gameObject.GetComponent<Custom.SceneManager>().SceneRoot.SetActive( true );

		switch ( Domain.Platform.PlatformCommType )
		{
			case CommType.TCP: 
			{ 
				comm = gameObject.GetComponent<TcpXmlClientCommProvider>();
				(comm as TcpXmlClientCommProvider).Initialize( 	Domain.Platform.UseLocalPlatform ? "localhost" : Domain.Platform.RemoteAddress, 
			               Domain.Platform.UseLocalPlatform ? Domain.Platform.LocalPort : Domain.Platform.RemotePort );
				break;
			}
			case CommType.HTTP: 
			{ 
				comm = gameObject.GetComponent<HttpJsonClientCommProvider>();
				(comm as HttpJsonClientCommProvider).Initialize( 	Domain.Platform.UseLocalPlatform ? "localhost" : Domain.Platform.RemoteAddress, 
			                Domain.Platform.UseLocalPlatform ? Domain.Platform.LocalPort : Domain.Platform.RemotePort );
				break;
			}
			case CommType.SIM:
			{
				comm = gameObject.GetComponent<SimClientCommProvider>();
				(comm as SimClientCommProvider).Initialize(Domain.Platform.UseLocalPlatform ? "localhost" : Domain.Platform.RemoteAddress,
							Domain.Platform.UseLocalPlatform ? Domain.Platform.LocalPort : Domain.Platform.RemotePort);
				break;
			}
		}

		comm.PlatformConnected += OnPlatformConnected;
		comm.Startup();
	}

	private void OnPlatformConnected()
	{
		Transition("GameInitState");
	}

	private void OnDisable()
	{
		comm.PlatformConnected -= OnPlatformConnected;
	}
}
