﻿using UnityEngine;
using LuxuryFruit.Library.Client;
using LuxuryFruit.Library.Core;
using LuxuryFruit.Library.Core.DTO;

public class GameInitState : StateBase 
{
	private IClientCommProvider comm = null;

	private void OnEnable()
	{
		comm = (IClientCommProvider)gameObject.GetComponent(typeof(IClientCommProvider));
		comm.InitResponse += OnInitResponse;
		comm.InitRequest(new InitRequestDTO{});
	}

	private void OnInitResponse( InitResponseDTO response )
	{
#if !UNITY_EDITOR
        // NOTE: Not necessary while loading from asset bundles. In fact it double loads and the UI digits disappear. Uncomment if planning on building without asset bundles turned on.
        //UnityEngine.SceneManagement.SceneManager.LoadScene("UI", UnityEngine.SceneManagement.LoadSceneMode.Additive);
#endif

        // Initialize machine constants
        Domain.Machine.MaxBetPerLine 		= response.max_bet_per_line;
		Domain.Machine.PaylineDefinitions 	= response.lines;
		Domain.Machine.ReelStripDefinitions = response.strips;

        // Initialize mutable game values
		Domain.Game.Credits 	= response.outcome.credits;
		Domain.Game.BetPerLine 	= response.outcome.bet.bet_per_line < 1 ? 1 : response.outcome.bet.bet_per_line;
		Domain.Game.BetLines 	= response.outcome.bet.num_lines < 1 ? 1 : response.outcome.bet.num_lines;
		Domain.Game.ReelStops 	= response.outcome.reel_stops;
		Domain.Game.Wins 		= response.outcome.wins;
		Domain.Game.SpinID		= response.outcome.id;

        Debug.Log("SpinID: " + Domain.Game.SpinID);
        Debug.Log("MaxBetPerLine: " + response.max_bet_per_line);
        Debug.Log("Credits: " + response.outcome.credits);

		if ( response.idle )
		{
			Debug.Log("Got response. Waiting for input.");
			Transition("WaitForInputState");
		}
		else
		{
			Debug.Log("Got response. Getting spin result.");
			Transition("GetSpinResultState"); //TODO: Show the spinning reels instead
		}

	}

	private void OnDisable()
	{
		if ( comm != null )
		{
			comm.InitResponse -= OnInitResponse;
			comm = null;
		}
	}
}
