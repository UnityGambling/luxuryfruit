﻿using UnityEngine;

public class InitLocalPlatformState : StateBase 
{
	LocalPlatformManager manager = null;

	private void OnEnable()
	{
		// Create the local platform process manager object that will live in the scene and persist across state transitions
		manager = gameObject.GetComponent<LocalPlatformManager>();
		if ( manager == null )
		{
			manager = gameObject.AddComponent<LocalPlatformManager>();
		}

		Transition("ConnectToPlatformState");
	}

	private void OnDisable()
	{

	}
}
