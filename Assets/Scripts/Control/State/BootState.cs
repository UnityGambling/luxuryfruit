﻿using UnityEngine;
using System.Collections;

public class BootState : StateBase
{
    public bool SkipConnectionUI = false;

#if UNITY_WEBCANVAS || UNITY_WEBGL
    private BootCanvasUI ui = null;
#else
	private BootUI ui = null;
#endif

	private void OnEnable()
	{
		gameObject.GetComponent<Custom.SceneManager>().SceneRoot.SetActive( false );

		Domain.Game.AllowInput = false;

		//TODO: Decide whether to use EditorBootUI or BootUI or WebBootUI or whatever
#if UNITY_WEBCANVAS || UNITY_WEBGL
        ui = gameObject.AddComponent<BootCanvasUI>();
#else
        ui = gameObject.AddComponent<BootUI>();
        if(SkipConnectionUI)
        {
            OnAdvance();
            return;
        }
#endif
		ui.Advance += OnAdvance;
	}

	private void OnAdvance()
	{
		switch ( Domain.Platform.PlatformCommType )
		{
			case CommType.TCP: { gameObject.AddComponent<TcpXmlClientCommProvider>(); break; }
			case CommType.HTTP: { gameObject.AddComponent<HttpJsonClientCommProvider>(); break; }
			case CommType.SIM: { gameObject.AddComponent<SimClientCommProvider>(); break; }
		}

		gameObject.AddComponent<PlatformPushReceiver> ();

		if ( Domain.Platform.UseLocalPlatform )
		{
			Transition("InitLocalPlatformState");
		}
		else
		{
			Transition("ConnectToPlatformState");
		}
	}

	private void OnDisable()
	{
		ui.Advance -= OnAdvance;
		Destroy( ui );
		ui = null;
	}
}
