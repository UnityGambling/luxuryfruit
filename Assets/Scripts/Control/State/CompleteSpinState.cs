﻿using UnityEngine;
using LuxuryFruit.Library.Client;
using LuxuryFruit.Library.Core;
using LuxuryFruit.Library.Core.DTO;

public class CompleteSpinState : StateBase 
{
	private IClientCommProvider comm = null;

	private void OnEnable()
	{
		comm = (IClientCommProvider)gameObject.GetComponent(typeof(IClientCommProvider));
		comm.CompleteResponse += OnCompleteResponse;
		comm.CompleteRequest(new CompleteRequestDTO { id = Domain.Game.SpinID } );
	}

	private void OnCompleteResponse( CompleteResponseDTO response )
	{
		//if ( !response.successful )
		//{
		//	Debug.LogWarning("Unable to complete a spin.");
		//	Transition("WaitForInputState");
		//	return;
		//}

		Domain.Game.SpinID = response.id;

		Transition("WaitForInputState");
	}

	private void OnDisable()
	{
		comm.CompleteResponse -= OnCompleteResponse;
		comm = null;
	}
}
