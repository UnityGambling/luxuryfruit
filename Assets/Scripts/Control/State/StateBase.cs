﻿using UnityEngine;
using System;
using System.Collections;

public abstract class StateBase : MonoBehaviour
{
	public virtual void Transition( String sb )
	{
		if (this == null)
		{
			Debug.Log("Transitioning from " + this.GetType().ToString() + " but I'm null!");
		}
		if (gameObject == null)
		{
			Debug.Log("Transitioning from " + this.GetType().ToString() + " but gameObject is null!");
		}

		// 
		if ( sb == "BootState" )
		{
			GameObject go = gameObject;
			DestroyImmediate(this);
			go.AddComponent< BootState >( );
		}
		if (sb == "CompleteSpinState")
		{
			GameObject go = gameObject;
			DestroyImmediate(this);
			go.AddComponent<CompleteSpinState>();
		}
		if (sb == "ConnectToPlatformState")
		{
			GameObject go = gameObject;
			DestroyImmediate(this);
			go.AddComponent<ConnectToPlatformState>();
		}
		if (sb == "GameInitState")
		{
			GameObject go = gameObject;
			DestroyImmediate(this);
			go.AddComponent<GameInitState>();
		}
		if (sb == "GetSpinResultState")
		{
			GameObject go = gameObject;
			DestroyImmediate(this);
			go.AddComponent<GetSpinResultState>();
		}
		if (sb == "InitLocalPlatformState")
		{
			GameObject go = gameObject;
			DestroyImmediate(this);
			go.AddComponent<InitLocalPlatformState>();
		}
		if (sb == "SpinState")
		{
			GameObject go = gameObject;
			DestroyImmediate(this);
			go.AddComponent<SpinState>();
		}
		if (sb == "WaitForInputState")
		{
			GameObject go = gameObject;
			DestroyImmediate(this);
			go.AddComponent<WaitForInputState>();
		}
	}
		
#if UNITY_EDITOR
	private string debugString = null;

	private void OnGUI()
	{
		if (string.IsNullOrEmpty(debugString))
		{
			debugString = "State=" + this.GetType().Name;
		}

		GUILayout.Label(debugString);
	}
#endif
}
