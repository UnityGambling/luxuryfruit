﻿using UnityEngine;
using System.Collections;
using AssetBundles;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class LoadGameScene : MonoBehaviour
{
    private enum EBundleLoadLocation
    {
        LOCAL_ASSET_SERVER = 0,
        LOCALHOST,
        WEB_SIDE_BY_SIDE,
        WEB_URL
    }

	public string[] sceneAssetBundles;
	public string[] sceneNames;
    public string symbolBundleName;
    public string[] symbolNames;
    public Canvas bundleSelectCanvas;
    public ToggleGroup gameSelectionToggles;
    public ToggleGroup resolutionSelectionToggles;
    public ToggleGroup bundleLocationToggles;
    public InputField bundleLoadAddress;

    private string[] activeVariants;
	private bool sd, hd, lf, newScene;
	private string whichGame = "lf", resolution = "sd";
    private EBundleLoadLocation eBundleLocation = EBundleLoadLocation.LOCAL_ASSET_SERVER;

	void Awake ()
	{
		activeVariants = new string[1];
	}

    void Start()
    {
        // Set up the inital state of the UI and store the values
        OnVariantPressed();
        OnBundleLocationTogglePressed();
    }
	
	// Use this for initialization
	IEnumerator BeginExample ()
	{
		yield return StartCoroutine(Initialize() );
		
		// Set active variants.
		AssetBundleManager.ActiveVariants = activeVariants;

		// Show the ActiveVariants in the console.
		Debug.Log (AssetBundleManager.ActiveVariants [0]);
		
		// Load variant level which depends on variants.
        for(int i=0; i < sceneAssetBundles.Length; i++)
		    yield return StartCoroutine(InitializeLevelAsync (sceneAssetBundles[i], sceneNames[i], true) );

        for (int i = 0; i < symbolNames.Length; i++)
            yield return StartCoroutine(InstantiateGameObjectAsync(symbolBundleName, symbolNames[i]));
    }

	// Initialize the downloading url and AssetBundleManifest object.
	protected IEnumerator Initialize()
	{
		// Don't destroy this gameObject as we depend on it to run the loading script.
		DontDestroyOnLoad(gameObject);

        if (eBundleLocation == EBundleLoadLocation.LOCAL_ASSET_SERVER)
            AssetBundleManager.SetDevelopmentAssetBundleServer();
		
        else if(eBundleLocation == EBundleLoadLocation.LOCALHOST || eBundleLocation == EBundleLoadLocation.WEB_URL)
            AssetBundleManager.SetSourceAssetBundleURL(bundleLoadAddress.text);
		
        // Use the following code if AssetBundles are side-by-side with web deployment:
        else if(eBundleLocation == EBundleLoadLocation.WEB_SIDE_BY_SIDE)
            AssetBundleManager.SetSourceAssetBundleURL(Application.dataPath + bundleLoadAddress.text);

        // Initialize AssetBundleManifest which loads the AssetBundleManifest object.
        var request = AssetBundleManager.Initialize();
		if (request != null)
			yield return StartCoroutine(request);
	}

	protected IEnumerator InitializeLevelAsync (string sceneAssetBundle, string levelName, bool isAdditive)
	{
		// This is simply to get the elapsed time for this phase of AssetLoading.
		float startTime = Time.realtimeSinceStartup;

        // Load level from assetBundle.
        AssetBundleLoadOperation request = AssetBundleManager.LoadLevelAsync(sceneAssetBundle, levelName, isAdditive);
        if (request == null)
            yield break;

        yield return StartCoroutine(request);

		// Calculate and display the elapsed time.
		float elapsedTime = Time.realtimeSinceStartup - startTime;
		Debug.Log("Finished loading scene " + levelName + " in " + elapsedTime + " seconds" );
	}

	protected IEnumerator InstantiateGameObjectAsync (string assetBundleName, string assetName)
	{
		// This is simply to get the elapsed time for this phase of AssetLoading.
		float startTime = Time.realtimeSinceStartup;
		
		// Load asset from assetBundle.
		AssetBundleLoadAssetOperation request = AssetBundleManager.LoadAssetAsync(assetBundleName, assetName, typeof(GameObject) );
		if (request == null)
		{
			Debug.LogError("Failed AssetBundleLoadAssetOperation on " + assetName + " from the AssetBundle " + assetBundleName + ".");
			yield break;
		}
		yield return StartCoroutine(request);
		
		// Get the Asset.
		GameObject prefab = request.GetAsset<GameObject> ();

		// Instantiate the Asset, or log an error.
		if (prefab != null)
			GameObject.Instantiate(prefab);
		else
			Debug.LogError("Failed to GetAsset from request");

		// Calculate and display the elapsed time.
		float elapsedTime = Time.realtimeSinceStartup - startTime;
		Debug.Log(assetName + (prefab == null ? " was not" : " was")+ " loaded successfully in " + elapsedTime + " seconds" );
	}

    /// <summary>
    /// Called when a toggle is pressed to set the bundle location to update the UI and allow the user to indicate from where bundles should be loaded.
    /// </summary>
    public void OnBundleLocationTogglePressed()
    {
        List<Toggle> activeToggle = new List<Toggle>(bundleLocationToggles.ActiveToggles());
        foreach(Toggle toggle in activeToggle)
        {
            if(toggle.name.Contains("LocalAssetServer"))
            {
                bundleLoadAddress.text = "";
                eBundleLocation = EBundleLoadLocation.LOCAL_ASSET_SERVER;
                break;
            } else if (toggle.name.Contains("Localhost"))
            {
                bundleLoadAddress.text = GetLastAddressOrDefault(EBundleLoadLocation.LOCALHOST);
                eBundleLocation = EBundleLoadLocation.LOCALHOST;
                break;
            }
            else if (toggle.name.Contains("WebSideBySide"))
            {
                bundleLoadAddress.text = GetLastAddressOrDefault(EBundleLoadLocation.WEB_SIDE_BY_SIDE);
                eBundleLocation = EBundleLoadLocation.WEB_SIDE_BY_SIDE;
                break;
            }
            else if (toggle.name.Contains("WebURL"))
            {
                bundleLoadAddress.text = GetLastAddressOrDefault(EBundleLoadLocation.WEB_URL);
                eBundleLocation = EBundleLoadLocation.WEB_URL;
                break;
            }
        }
    }

    /// <summary>
    /// Get the address to fill in the text box as a helper. Checks to see if there is one stored from 
    /// the previous run so the user doesn't have to fill it in every time.
    /// </summary>
    /// <param name="locationType"> Indicates which location to retrieve </param>
    /// <returns></returns>
    private string GetLastAddressOrDefault(EBundleLoadLocation locationType)
    {
        switch(locationType)
        {
            case EBundleLoadLocation.LOCALHOST:
                return PlayerPrefs.GetString("localhostAddress") == "" ? "http://localhost/LuxuryFruit/AssetBundles/" : PlayerPrefs.GetString("localhostAddress");
            case EBundleLoadLocation.WEB_SIDE_BY_SIDE:
                return PlayerPrefs.GetString("webSideBySide") == "" ? "/" : PlayerPrefs.GetString("webSideBySide");
            case EBundleLoadLocation.WEB_URL:
                return PlayerPrefs.GetString("weburl") == "" ? "http://" : PlayerPrefs.GetString("weburl");
			default:
				break;
        }
        return "";
    }

    /// <summary>
    /// Called when a variant toggle is pressed to update the variant that will be loaded
    /// </summary>
    public void OnVariantPressed()
    {
        List<Toggle> activeGameToggle = new List<Toggle>(gameSelectionToggles.ActiveToggles());
        List<Toggle> activeResolutionToggle = new List<Toggle>(resolutionSelectionToggles.ActiveToggles());
        foreach (Toggle toggle in activeGameToggle)
        {
            if (toggle.name.Contains("LF"))
            {
                whichGame = "lf";
                break;
            }
            else if (toggle.name.Contains("New"))
            {
                whichGame = "new";
                break;
            }
        }
        foreach(Toggle toggle in activeResolutionToggle)
        {
            if (toggle.name.Contains("HD"))
            {
                resolution = "hd";
                break;
            }
            else if (toggle.name.Contains("SD"))
            {
                resolution = "sd";
                break;
            }
        }
    }

    /// <summary>
    /// Called when the Load Game button is pressed which kicks off loading asset bundles
    /// </summary>
    public void OnLoadGamePressed()
    {
        SaveSelectionData();

        // Set the activeVariant
        activeVariants[0] = whichGame + "-" + resolution;
        //Show this in the log to make sure it is correct
        Debug.Log(activeVariants[0]);
        // Load the scene now!
        StartCoroutine(BeginExample());

		Destroy(bundleSelectCanvas.gameObject);
    }

    /// <summary>
    /// Store the data that has been selected so far so the user can more easily breeze through the initial screen
    /// </summary>
    private void SaveSelectionData()
    {
        PlayerPrefs.SetString("game", whichGame);
        PlayerPrefs.SetString("resolution", resolution);
        if(eBundleLocation == EBundleLoadLocation.LOCALHOST)
            PlayerPrefs.SetString("localhostAddress", bundleLoadAddress.text);
        else if(eBundleLocation == EBundleLoadLocation.WEB_SIDE_BY_SIDE)
            PlayerPrefs.SetString("webSideBySide", bundleLoadAddress.text);
        else if(eBundleLocation == EBundleLoadLocation.WEB_URL)
            PlayerPrefs.SetString("weburl", bundleLoadAddress.text);
    }
}
