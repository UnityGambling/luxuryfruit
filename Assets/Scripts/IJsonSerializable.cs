﻿

public interface IJsonSerializable
{
    JSONObject ToJsonObject();
}