﻿using UnityEngine;
using System.Collections;

public class SymbolDefinitionAsset : ScriptableObject 
{
	public string DisplayName = null;
	public Sprite ReelSprite = null;

	/*
	public AudioClip[] Sounds = null;
	public AnimationClip[] Animations = null;
	*/
}
