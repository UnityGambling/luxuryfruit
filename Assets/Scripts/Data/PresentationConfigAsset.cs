﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PresentationConfigAsset : ScriptableObject 
{
	public string DisplayName = null;
	public List<SymbolDefinitionAsset> SymbolDefinitions = null;

}
