﻿using UnityEngine;
using System.Collections;

public class DigitalSpriteFontAsset : ScriptableObject 
{
	public Sprite[] Digits = null;
}
