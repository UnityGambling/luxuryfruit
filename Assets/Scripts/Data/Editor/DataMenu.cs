﻿using UnityEngine;
using UnityEditor;
using System;

public class DataMenu
{
	[MenuItem("Assets/Create/Luxury Fruit/Symbol Definition")]
	public static void CreateSymbolDefinition()
	{
		CustomAssetUtility.CreateAsset<SymbolDefinitionAsset>();
	}

	[MenuItem("Assets/Create/Luxury Fruit/Presentation Config")]
	public static void CreatePresentationConfig()
	{
		CustomAssetUtility.CreateAsset<PresentationConfigAsset>();
	}

	[MenuItem("Assets/Create/Luxury Fruit/Digital Sprite Font")]
	public static void CreateDigitalSpriteFont()
	{
		CustomAssetUtility.CreateAsset<DigitalSpriteFontAsset>();
	}
}