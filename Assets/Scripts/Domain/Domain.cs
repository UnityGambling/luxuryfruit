﻿using UnityEngine;
using System.Collections;

//Note that static classes and values survive scene changes and other destructive 
//events in Unity and need to be manually reset if you want them cleared
public static class Domain 
{
	public static PlatformDO Platform 	= new PlatformDO();
	public static MachineDO Machine 	= new MachineDO();
	public static GameDO Game 			= new GameDO();
	public static ConfigDO Config		= new ConfigDO();
}
