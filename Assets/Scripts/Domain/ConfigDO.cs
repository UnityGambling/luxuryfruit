﻿using System;

public class ConfigDO 
{
    public event Action Change;

    //TODO: More properties in presentation config, also look into several samples w/automated build
    private PresentationConfigAsset presentation = null;

    public PresentationConfigAsset Presentation
    {
        get { return presentation; }
        set { presentation = value; if (Change != null) { Change(); } }
    }


	
	//public Property<PresentationConfigAsset> Presentation = new Property<PresentationConfigAsset>();
}
