﻿using System;

public class MachineDO
{
    public event Action Change;
    public event Action ReelStripDefinitionsChange;
    public event Action PaylineDefinitionsChange;

    private double maxBetPerLine = 0;

    public double MaxBetPerLine
    {
        get { return maxBetPerLine; }
        set { maxBetPerLine = value; if (Change != null) { Change(); } }
    }

    private long[][] paylineDefinitions = null;

    public long[][] PaylineDefinitions
    {
        get { return paylineDefinitions; }
        set { paylineDefinitions = value; if (Change != null) { Change(); } if (PaylineDefinitionsChange != null) { PaylineDefinitionsChange(); } }
    }

    private long[][] reelStripDefinitions = null;

    public long[][] ReelStripDefinitions
    {
        get { return reelStripDefinitions; }
        set { reelStripDefinitions = value; if (Change != null) { Change(); } if (ReelStripDefinitionsChange != null) { ReelStripDefinitionsChange(); } }
    } 

    
}
