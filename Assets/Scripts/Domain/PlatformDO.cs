﻿using System;

public class PlatformDO 
{
    public event Action Change;

    private CommType platformCommType = CommType.TCP;

    public CommType PlatformCommType
    {
        get { return platformCommType; }
        set { platformCommType = value; if (Change != null) { Change(); } }
    }

    private bool useLocalPlatform = true;

    public bool UseLocalPlatform
    {
        get { return useLocalPlatform; }
        set { useLocalPlatform = value; if (Change != null) { Change(); } }
    }

    private int localPort = 80;

    public int LocalPort
    {
        get { return localPort; }
        set { localPort = value; if (Change != null) { Change(); } }
    }

    private int remotePort = 80;

    public int RemotePort
    {
        get { return remotePort; }
        set { remotePort = value; if (Change != null) { Change(); } }
    }

    private string remoteAddress = "localhost";

    public string RemoteAddress
    {
        get { return remoteAddress; }
        set { remoteAddress = value; if (Change != null) { Change(); } }
    }

}
