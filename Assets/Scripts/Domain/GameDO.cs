﻿using System;
using LuxuryFruit.Library.Core.DTO;

public class GameDO
{
    public event Action Change;
    public event Action ReelStopsChange;
    public event Action WinsChange;
    public event Action BetLinesChange;
    public event Action CreditsChange;
    public event Action BetPerLineChange;

    private bool allowInput = false;

    public bool AllowInput
    {
        get { return allowInput; }
        set { allowInput = value; if (Change != null) { Change(); } }
    }
    private double credits = 0;

    public double Credits
    {
        get { return credits; }
        set { credits = value; if (Change != null) { Change(); } if (CreditsChange != null) { CreditsChange(); } }
    }
    private long betLines = 0;

    public long BetLines
    {
        get { return betLines; }
        set { betLines = value; if (Change != null) { Change(); } if (BetLinesChange != null) { BetLinesChange(); } }
    }
    private double betPerLine = 0;

    public double BetPerLine
    {
        get { return betPerLine; }
        set { betPerLine = value; if (Change != null) { Change(); } if (BetPerLineChange != null) { BetPerLineChange(); } }
    }
    private long[] reelStops = null;

    public long[] ReelStops
    {
        get { return reelStops; }
        set { reelStops = value; if (Change != null) { Change(); } if (ReelStopsChange != null) { ReelStopsChange(); } }
    }
    private LineWinDTO[] wins = null;

    public LineWinDTO[] Wins
    {
        get { return wins; }
        set { wins = value; if (Change != null) { Change(); } if (WinsChange != null) { WinsChange(); } }
    }
    private long spinID = 0;

    public long SpinID
    {
        get { return spinID; }
        set { spinID = value; if (Change != null) { Change(); } }
    }
}
