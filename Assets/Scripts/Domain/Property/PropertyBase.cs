﻿using System;

public class PropertyBase
{
	// Subscriptions to this event do not prevent the PropertyBase from being garbage collected
	public event Action Changed;
	
	protected void Notify()
	{
		if (Changed == null)
		{
			return;
		}
		Changed.Invoke();
	}
}
