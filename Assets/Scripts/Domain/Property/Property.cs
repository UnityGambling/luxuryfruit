﻿using System;

public class Property<T> : PropertyBase
{
	private T val = default(T);
	
	public Property() { }
	
	public Property(T initialValue)
	{
		val = initialValue;
	}
	
	public T Value
	{
		get { return val; }
		set
		{
			if (val == null && value != null || !val.Equals(value))
			{
				val = value;
				Notify();
			}
		}
	}
	
	public static implicit operator T(Property<T> p)
	{
		return p.Value;
	}
}