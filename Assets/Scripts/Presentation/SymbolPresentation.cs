﻿using UnityEngine;
using System.Collections;

public class SymbolPresentation : MonoBehaviour 
{
	public SpriteRenderer SymbolSprite = null;
	public GameObject HighlightObject = null;

	public long Index = 0;

	private void OnEnable()
	{
		HighlightObject.SetActive(false);
	}

	private void OnDisable()
	{

	}

	public void SetColor( Color color )
	{
		HighlightObject.GetComponent<SpriteRenderer>().color = color;
	}

	public void Highlight( bool enable )
	{
		HighlightObject.SetActive( enable );
	}

	public void SetSymbolDisplayID( long id )
	{
		SymbolSprite.sprite = Domain.Config.Presentation.SymbolDefinitions[(int)id].ReelSprite;
	}
}
