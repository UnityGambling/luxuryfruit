﻿using System;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class ButtonPresentation : MonoBehaviour 
{
	public event Action Click;
	public event Action Press;

	private void OnMouseUp()
	{
		if (Click != null)
		{
			Click();
		}
	}
	
	private void OnMouseDown()
	{
		if (Press != null)
		{
			Press();
		}
	}
}
