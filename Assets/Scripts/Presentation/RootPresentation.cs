﻿using UnityEngine;
using System.Collections;

public class RootPresentation : MonoBehaviour 
{
    public static RootPresentation instance = null;

    public PanelPresentation Panel = null;
	public ReelSetPresentation Reels = null;
	public LineSetPresentation Lines = null;

    void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
    }
}
