﻿using System;
using UnityEngine;
using System.Collections;
using LuxuryFruit.Library.Core.DTO;

public class ReelSetPresentation : MonoBehaviour 
{
	public event Action SpinPresentationComplete;

	public float SpinSpeed = 10f;
	public float SpinTime = 2f;
	public float SpinStagger = 0.5f;

	public float SymbolHighlightTime = 2f;
	public float VerticalSymbolSpace = 1.4f; 	// Vertical space between symbols //TODO: Just get the spriterenderer.bounds.y
	public int SymbolCenterIndex = 2;			// Which symbol index corresponds to a 0 payline offset

	public ReelPresentation[] Reels = null;

	private LineSetPresentation lines = null;

	private bool firstStops = true;
	private bool spinning = false;

	private void Awake()
	{
		foreach( ReelPresentation reel in Reels )
		{
			reel.Configure( SymbolCenterIndex, VerticalSymbolSpace, SpinSpeed );
		}
	}

	private void OnEnable()
	{
		Domain.Machine.ReelStripDefinitionsChange += OnReelStripDefinitionsChanged;
        Domain.Game.BetLinesChange += OnBetLinesChanged;
		Domain.Game.WinsChange += OnWinsChanged;
        Domain.Game.ReelStopsChange += OnReelStopsChanged;
		
		lines = GameObject.Find("root").GetComponent<RootPresentation>().Lines;
	}

	private void OnDisable()
	{
        Domain.Game.ReelStopsChange -= OnReelStopsChanged;
        Domain.Game.WinsChange -= OnWinsChanged;
        Domain.Game.BetLinesChange -= OnBetLinesChanged;
        Domain.Machine.ReelStripDefinitionsChange -= OnReelStripDefinitionsChanged;
        
	}

	public bool Spinning
	{
		get
		{
			return spinning;
		}
	}

	public Vector3 GetMainLineOrigin()
	{
		//TODO: Use vertical offset calculated from height
		return Reels[0].Symbols[0].transform.position + (Reels[0].Symbols[1].transform.position - Reels[0].Symbols[0].transform.position) * SymbolCenterIndex;
	}

	//TODO: Gives an X-Y delta between symbols. Make this more clear
	public Vector3 GetSymbolSeparation()
	{
		return (Reels[0].Symbols[1].transform.position - Reels[0].Symbols[0].transform.position) + (Reels[1].Symbols[0].transform.position - Reels[0].Symbols[0].transform.position);
	}

    private void OnWinsChanged()
	{
		if (!spinning)
		{
			StartCoroutine(AnimateWinsRoutine());
		}
	}

	private IEnumerator AnimateWinsRoutine()
	{
		LineSetPresentation lines = GameObject.Find("root").GetComponent<RootPresentation>().Lines;

		LineWinDTO[] wins = Domain.Game.Wins;
		long[][] paylines = Domain.Machine.PaylineDefinitions;

		while (true)
		{
			yield return null;

			for ( int i = 0; i < wins.Length; ++i )
			{
				long lineIndex = wins[i].line_index;
				ClearHighlights();
				HighlightPaylineSymbols( paylines[ lineIndex ], wins[i].symbols, lines.LineColors[lineIndex] );
				lines.ClearHighlights();
				lines.Highlight( lineIndex, true, wins[i].symbols );

				yield return new WaitForSeconds( SymbolHighlightTime );
			}
		}
	}

	private void OnReelStripDefinitionsChanged()
	{
		long[][] stripDefinitions = Domain.Machine.ReelStripDefinitions;

		for (int i = 0; i < Reels.Length; ++i )
		{
			Reels[i].Initialize( stripDefinitions[i] );
		}
	}

	private void HighlightPaylineSymbols( long[] offsets, bool[] symbols, Color lineColor )
	{
		for ( int i = 0; i < Reels.Length; ++i )
		{
			Reels[i].HighlightOffset( offsets[i], symbols[i], lineColor );
		}
	}

	private void ClearHighlights()
	{
		foreach ( ReelPresentation reel in Reels )
		{
			reel.ClearHighlights();
		}
	}

	private void OnBetLinesChanged()
	{
		StopAllCoroutines();
		ClearHighlights();
	}
	
	private IEnumerator SpinRoutine()
	{
		spinning = true;

		foreach ( ReelPresentation reel in Reels )
		{
            reel.Configure(SymbolCenterIndex, VerticalSymbolSpace, SpinSpeed);
			reel.Spin();
		}

		yield return new WaitForSeconds( SpinTime );

		foreach ( ReelPresentation reel in Reels )
		{
			reel.Stop();
			while (reel.Spinning)
			{
				yield return null;
			}
			yield return new WaitForSeconds( SpinStagger );
		}

		spinning = false;
		OnWinsChanged();

		if ( SpinPresentationComplete != null )
		{
			SpinPresentationComplete();
		}
	}

	private void OnReelStopsChanged()
	{
		if ( !firstStops )
		{
			StopAllCoroutines();
			ClearHighlights();
			lines.ClearHighlights();
		
			StartCoroutine(SpinRoutine());
		}
		firstStops = false;

		long[] stops = Domain.Game.ReelStops;
		if ( stops.Length < Reels.Length )
		{
            return;
			//throw new InvalidOperationException(string.Format("Received an unexpected number of reel stops: {0} expected {1} actual", Reels.Length, stops.Length));
		}

		for (int i = 0; i < Reels.Length; ++i )
		{
			Reels[i].SetStop( stops[i] );
		}
	}
}
