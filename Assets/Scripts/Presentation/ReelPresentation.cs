﻿using UnityEngine;
using System.Collections;

public class ReelPresentation : MonoBehaviour 
{
	private long[] symbolIDs;
	private float verticalSymbolSpace = 0f; // TODO: Use this when animating/cycling symbols during a spin
	private int symbolCenterIndex = 2;
	private float scrollSpeed = 10f;

	public SymbolPresentation[] Symbols = null;
	
	private long stopIndex = 0;

	private Vector3 top;
	private Vector3 bottom;

	private bool firstStop = true;
	private bool spinning = false;
	private bool stop = true;

	private Vector3[] startPositions;

	private void Awake()
	{
		top = Symbols[0].transform.position;
		bottom = Symbols[3].transform.position + (Symbols[3].transform.position - Symbols[2].transform.position);
		startPositions = new Vector3[] { Symbols[0].transform.position, Symbols[1].transform.position, Symbols[2].transform.position, Symbols[3].transform.position };
	}

	private void OnEnable()
	{
	}

	private void OnDisable()
	{
	}

	public void HighlightOffset( long offset, bool highlight, Color color )
	{
		SymbolPresentation symbol = Symbols[ symbolCenterIndex + offset ];
		symbol.SetColor( color );
		symbol.Highlight( highlight );
	}

	public void ClearHighlights()
	{
		foreach ( SymbolPresentation symbol in Symbols)
		{
			symbol.Highlight(false);
		}
	}

	private int ArrayMod(long x, int m)
	{
		return (int)((x % m + m) % m);
	}

	public void SetStop( long index )
	{
		stopIndex = index;
		if (!spinning)
		{
			Stop();
		}
	}

	public void Spin()
	{
		StopAllCoroutines();
		StartCoroutine(SpinRoutine());
	}

	public bool Spinning
	{
		get { return spinning; }
	}

	private IEnumerator SpinRoutine()
	{
		stop = false;
		spinning = true;

		bool running = true;
		while (running)
		{
			yield return null;
			for ( int i = 0; i < Symbols.Length; ++i )
			{
				SymbolPresentation symbol = Symbols[i];
				symbol.transform.Translate( Vector3.down * scrollSpeed * Time.deltaTime );
				float delta = bottom.y - symbol.transform.position.y;
				if ( delta >= 0 )
				{
					symbol.transform.position = top;
					symbol.Index = ArrayMod( symbol.Index - Symbols.Length, symbolIDs.Length );
					symbol.SetSymbolDisplayID( symbolIDs[ symbol.Index ] );
					if ( stop && symbol.Index == ArrayMod( stopIndex - symbolCenterIndex, symbolIDs.Length ) )
					{
						running = false;
						break;
					}
				}
			}
		}

		spinning = false;

		UpdateSymbolsFromStops();
	}

	public void UpdateSymbolsFromStops()
	{
		for (int i = 0; i < Symbols.Length; ++i)
		{
			Symbols[i].transform.position = startPositions[i];
			Symbols[i].Index = ArrayMod( stopIndex + i - symbolCenterIndex, symbolIDs.Length );
			Symbols[i].SetSymbolDisplayID( symbolIDs[ Symbols[i].Index ] );
		}
	}

	public void Stop()
	{
		stop = true;

		if ( firstStop )
		{
			firstStop = false;
			UpdateSymbolsFromStops();
		}
	}

	public void Configure( int centerIndex, float verticalSpace, float spinSpeed )
	{
		symbolCenterIndex = centerIndex;
		verticalSymbolSpace = verticalSpace;
		scrollSpeed = spinSpeed;
	}

	public void Initialize( long[] symbolIDs )
	{
		this.symbolIDs = symbolIDs;
		//TODO: Add support for spinning the reels and changing the reels, etc
	}
}

