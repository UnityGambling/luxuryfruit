﻿using System;
using UnityEngine;


public class BootCanvasUI : MonoBehaviour 
{
	public event Action Advance;

    private bool advanced = false;

	private void OnEnable()
	{
        //TODO: We need to pull the configuration from the web page through the javascript bridge
        Domain.Platform.PlatformCommType = CommType.SIM;
        Domain.Platform.UseLocalPlatform = false;
        Domain.Platform.RemotePort = 8080;
        Domain.Platform.RemoteAddress = "10.65.1.75";
	}

    private void Update()
    {
        if (advanced) { return; }

        if (Advance != null)
        {
            advanced = true;
            Advance();
        }
    }
}
