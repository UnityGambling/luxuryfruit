﻿using System;
using UnityEngine;


public class BootUI : MonoBehaviour 
{
	public event Action Advance;

	private const int UIWidth = 300;
	private const int UIHeight = 125;
	private const int PortFieldWidth = 75;
	private readonly string[] PlatformOptionTexts = new string[]{"Sim", "Local", "Remote"};
	private readonly string[] CommOptionTexts = new string[]{"TCP", "HTTP"};

	private const string PrefKeyCommType 			= "platform_comm_type";
	private const string PrefKeyPlatformLocation 	= "platform_location";
	private const string PrefKeyLocalPort 			= "platform_local_port";
	private const string PrefKeyRemotePort 			= "platform_remote_port";
	private const string PrefKeyRemoteAddress 		= "platform_remote_address";

	private const int CommTypeIndexSim = 0;
	private const int CommTypeIndexTcp = 1;
	private const int CommTypeIndexHttp = 2;

    private const int LocationIndexSim = 0;
    private const int LocationIndexLocal  = 1;
	private const int LocationIndexRemote = 2;

	private int commTypeIndex			= 0;
	private int platformLocationIndex 	= LocationIndexSim;
	private int localPort 				= 80;
	private int remotePort 				= 80;
	private string remoteAddress 		= "10.65.1.75";

	private void OnEnable()
	{
		// If there is no pref with this key (first run), set up all the prefs
		if (!PlayerPrefs.HasKey(PrefKeyRemoteAddress)) 
		{ 
			SetDomainAndPrefs();
		}

		// Get any prefs that were saved during previous executions
		commTypeIndex 			= PlayerPrefs.GetInt(PrefKeyCommType);
		platformLocationIndex 	= PlayerPrefs.GetInt(PrefKeyPlatformLocation);
		localPort 				= PlayerPrefs.GetInt(PrefKeyLocalPort);
		remotePort 				= PlayerPrefs.GetInt(PrefKeyRemotePort);
		remoteAddress 			= PlayerPrefs.GetString(PrefKeyRemoteAddress);

        SetDomain();
    }

	private void SetDomainAndPrefs()
	{
		PlayerPrefs.SetInt(PrefKeyCommType, commTypeIndex);
		PlayerPrefs.SetInt(PrefKeyPlatformLocation, platformLocationIndex); 
		PlayerPrefs.SetInt(PrefKeyLocalPort, localPort);
		PlayerPrefs.SetInt(PrefKeyRemotePort, remotePort);
		PlayerPrefs.SetString(PrefKeyRemoteAddress, remoteAddress);

        SetDomain();
	}

    private void SetDomain()
    {
        Domain.Platform.PlatformCommType = CommType.SIM;    // (commTypeIndex == 0 ? CommType.SIM : (commTypeIndex == 1 ? CommType.TCP : CommType.HTTP));
        Domain.Platform.UseLocalPlatform = (platformLocationIndex == LocationIndexLocal);
        Domain.Platform.LocalPort = localPort;
        Domain.Platform.RemotePort = remotePort;
        Domain.Platform.RemoteAddress = remoteAddress;
    }

	private void OnGUI()
	{
		int width = Screen.width;
		int height = Screen.height;

		GUILayout.BeginArea(new Rect((width-UIWidth)/2, (height-UIHeight)/2, UIWidth, UIHeight), GUI.skin.GetStyle("box"));
		GUILayout.BeginVertical();

		int oldLocationIndex = platformLocationIndex;

        GUILayout.BeginHorizontal();
        GUILayout.Label("Platform");
        platformLocationIndex = GUILayout.SelectionGrid(platformLocationIndex, PlatformOptionTexts, 3);
        GUILayout.EndHorizontal();

        if(platformLocationIndex != LocationIndexSim)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Comm Type");
            commTypeIndex = GUILayout.SelectionGrid(commTypeIndex, CommOptionTexts, 2);
            GUILayout.EndHorizontal();

            if (oldLocationIndex != platformLocationIndex)
            {
                // If we switched options, make sure to persist anything we typed in.
                SetDomainAndPrefs();
            }

            GUILayout.BeginHorizontal();
            if (platformLocationIndex == 0)
            {
                GUILayout.Label("Port");
                string portString = GUILayout.TextField(localPort.ToString());
                int parsed = 0;
                if (int.TryParse(portString, out parsed))
                {
                    localPort = parsed;
                }
            }
            else
            {
                GUILayout.Label("Address");
                remoteAddress = GUILayout.TextField(remoteAddress);
                GUILayout.Label("Port");
                string portString = GUILayout.TextField(remotePort.ToString());
                int parsed = 0;
                if (int.TryParse(portString, out parsed))
                {
                    remotePort = parsed;
                }
            }
            GUILayout.EndHorizontal();
        }

		GUILayout.Space(15);

		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		if ( GUILayout.Button("OK") && Advance != null )
		{
			SetDomainAndPrefs();
			Advance();
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();

		GUILayout.EndVertical();
		GUILayout.EndArea();
	}

	private void OnDisable()
	{

	}
}
