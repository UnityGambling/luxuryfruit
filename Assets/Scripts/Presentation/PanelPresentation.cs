﻿using System;
using UnityEngine;
using System.Collections;
using LuxuryFruit.Library.Core.DTO;

public class PanelPresentation : MonoBehaviour 
{
	public event Action Spin;

	private LineSetPresentation lines = null;

	private void OnEnable()
	{
		lines = GameObject.Find("root").GetComponent<RootPresentation>().Lines;

        Domain.Game.BetLinesChange      += OnBetLinesChanged;
	}

	private void OnDisable()
	{
        Domain.Game.BetLinesChange -= OnBetLinesChanged;
	}

    public void OnBetLinesChanged()
	{
        lines.ClearHighlights();
        for (int i = 0; i < Domain.Machine.PaylineDefinitions.Length; ++i)
        {
            lines.Highlight(i, i < Domain.Game.BetLines, null);
        }
    }

	public void OnSpinButtonClick()
	{
		if (!Domain.Game.AllowInput) { return; }

		if ( Spin != null )
		{
			Spin();
		}
	}

	public void OnBetMaxButtonClick()
	{
		if (!Domain.Game.AllowInput) { return; }
        
		if (Spin != null)
		{
			Spin();
		}
	}
}
