﻿using UnityEngine;
using System.Collections;

public class PresentationConfig : MonoBehaviour {

	public PresentationConfigAsset ConfigAsset = null;

	private void Awake()
	{
		Domain.Config.Presentation = ConfigAsset;
	}
}
