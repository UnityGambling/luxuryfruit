﻿using UnityEngine;
using System.Collections;

public class LinePresentation : MonoBehaviour 
{
	private long[]  symbolOffsets 		= null;
	private Vector3 mainLineOrigin		= Vector3.zero;
	private Vector3 symbolSeparation	= Vector3.zero;
	private Color 	lineColor 			= Color.white;
	private Sprite 	lineBackground 		= null;
	private Sprite 	lineForeground 		= null;
	private string 	sortingLayer 		= string.Empty;
	private int 	sortingOrder		= 0;

	private void OnEnable()
	{

	}

	private void OnDisable()
	{

	}

	public void Initialize( long[] symbolOffsets, Vector3 mainLineOrigin, Vector3 symbolSeparation, Color lineColor, Sprite lineBackground, Sprite lineForeground, string sortingLayer, int sortingOrder )
	{
		this.symbolOffsets		= symbolOffsets;
		this.mainLineOrigin		= mainLineOrigin;
		this.symbolSeparation 	= symbolSeparation;
		this.lineColor			= lineColor;
		this.lineBackground 	= lineBackground;
		this.lineForeground 	= lineForeground;
		this.sortingLayer		= sortingLayer;
		this.sortingOrder		= sortingOrder;
	}

	public void Highlight( bool enabled, bool[] wins )
	{
		while (transform.childCount > 0)
		{
			DestroyImmediate(transform.GetChild(0).gameObject);
		}
		
		gameObject.SetActive( enabled );
		
		if ( !enabled ) { return; }

		for ( int i = -1; i < symbolOffsets.Length; ++i )
		{
			bool fromHighlight 	= wins != null && i >= 0 && wins[ i ];
			bool toHighlight 	= wins != null && ( i + 1 ) < symbolOffsets.Length && wins[ i + 1 ];
			CreateSpan( i, i + 1, fromHighlight, toHighlight );
		}
	}

	private void CreateSpan( int fromIndex, int toIndex, bool fromHighlight, bool toHighlight )
	{
		Vector3 start	= SymbolCenter( fromIndex );
		Vector3 end 	= SymbolCenter( toIndex );
		Vector3 delta 	= end - start;
		
		ClipLineAgainstSymbolHighlights( ref start, ref end, SymbolBounds( fromIndex ), SymbolBounds( toIndex ), fromHighlight, toHighlight );
		
		GameObject span = new GameObject();
		span.transform.parent = transform;
		span.transform.position = start;
		span.transform.rotation = Quaternion.identity;
		
		CreateLineSprite( lineBackground, span, sortingOrder, start, end );
		CreateLineSprite( lineForeground, span, sortingOrder + 1, start, end );
		
		span.transform.right = delta.normalized;
	}

	private Bounds SymbolBounds( int symbolIndex )
	{
		return new Bounds( SymbolCenter( symbolIndex ), symbolSeparation );
	}
	
	private Vector3 SymbolCenter( int symbolIndex )
	{
		return mainLineOrigin 
			+ ( Vector3.up * symbolOffsets[ Mathf.Max( 0, Mathf.Min( symbolOffsets.Length - 1 , symbolIndex)  ) ] * symbolSeparation.y ) 
				+ ( Vector3.right * symbolIndex * symbolSeparation.x );
	}

	private void CreateLineSprite( Sprite sprite, GameObject lineParent, int sort, Vector3 start, Vector3 end )
	{
		GameObject line = new GameObject();
		line.transform.parent = lineParent.transform;
		line.transform.localPosition = Vector3.zero;
		line.transform.localRotation = Quaternion.identity;
		
		SpriteRenderer lineSprite = line.AddComponent<SpriteRenderer>();
		lineSprite.color = lineColor;
		lineSprite.sortingLayerName = sortingLayer;
		lineSprite.sortingOrder = sort;
		lineSprite.sprite = sprite;
		
		line.transform.localScale = new Vector3( ( end - start ).magnitude / lineSprite.bounds.size.x, 1f, 1f );
	}

	
	private void ClipLineAgainstSymbolHighlights( ref Vector3 start, ref Vector3 end, Bounds fromHighlightBounds, Bounds toHighlightBounds, bool fromHighlight, bool toHighlight )
	{
		Vector3 enterLocation;
		Vector3 exitLocation;
		
		//Test against origin symbol
		if ( fromHighlight && LineBoundsIntersect2D( start, end, fromHighlightBounds, out enterLocation, out exitLocation ) )
		{
			start = exitLocation;
		}
		//Test against destination symbol
		if ( toHighlight && LineBoundsIntersect2D( start, end, toHighlightBounds, out enterLocation, out exitLocation ) )
		{
			end = enterLocation;
		}
	}

	private bool LineBoundsIntersect2D( Vector3 start, Vector3 end, Bounds bounds, out Vector3 hitLocation, out Vector3 exitLocation )
	{
		hitLocation = Vector3.zero;
		exitLocation = Vector3.zero;
		
		float       enter = 0.0f;
		float       exit = 1.0f;
		Vector3    	delta = end - start;
		
		if( !PointLineIntersect1D( start.x, delta.x, bounds.min.x, bounds.max.x, ref enter, ref exit)) { return false; }
		if( !PointLineIntersect1D( start.y, delta.y, bounds.max.y, bounds.min.y, ref enter, ref exit)) { return false; } //max and min are intentionally flipped here for the y dimension
		
		hitLocation = start + delta * enter;
		exitLocation = start + delta * exit;
		return true;
	}
	
	bool PointLineIntersect1D( float start, float dir, float min, float max, ref float enter, ref float exit )
	{
		if ( Mathf.Abs( dir ) < 1.0E-8 )
		{
			return ( start >= min && start <= max );
		}
		
		float   ooDir = 1.0f / dir;
		float   t0 = ( min - start ) * ooDir;
		float   t1 = ( max - start ) * ooDir;
		
		if ( t0 > t1 )
		{
			float temp = t0;
			t0 = t1;
			t1 = temp;
		}
		
		if ( t0 > exit || t1 < enter )
		{
			return false;
		}
		
		if ( t0 > enter ) { enter = t0; }
		
		if ( t1 < exit ) { exit = t1; }
		
		return true;
	}
}
