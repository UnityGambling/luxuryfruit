﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LineSetPresentation : MonoBehaviour 
{
	public string SortingLayerName = "Front";
	public int SortingOrder = 1;

	public Sprite LineBackground = null;
	public Sprite LineForeground = null;

	public Color[] LineColors		= null;

	private ReelSetPresentation reels = null;

	private List<LinePresentation> lines = new List<LinePresentation>();

	private void OnEnable()
	{
		reels = GameObject.Find("root").GetComponent<RootPresentation>().Reels;
		Domain.Machine.PaylineDefinitionsChange += OnPaylineDefinitionsChanged;
	}

	private void OnDisable()
	{
		Domain.Machine.PaylineDefinitionsChange -= OnPaylineDefinitionsChanged;
	}

	private void OnPaylineDefinitionsChanged()
	{
		lines = new List<LinePresentation>();

		foreach ( Transform xform in transform )
		{
			Destroy( xform );
		}

		int lineNum = 0;
		Vector3 origin = reels.GetMainLineOrigin();
		Vector3 separation = reels.GetSymbolSeparation();
		foreach ( long[] offsets in Domain.Machine.PaylineDefinitions )
		{
			lines.Add( CreateLine( offsets, origin, separation, LineColors[lineNum] ) );
			++lineNum;
		}

		ClearHighlights();
	}

	private LinePresentation CreateLine( long[] offsets, Vector3 mainLineOrigin, Vector3 symbolSeparation, Color lineColor )
	{
		GameObject go = new GameObject();
		go.transform.parent = transform;
		go.transform.position = Vector3.zero;
		go.transform.rotation = Quaternion.identity;
		LinePresentation lp = go.AddComponent<LinePresentation>();
		lp.Initialize( offsets, mainLineOrigin, symbolSeparation, lineColor, LineBackground, LineForeground, SortingLayerName, SortingOrder );
		return lp;
	}

	public void ClearHighlights()
	{
		foreach( LinePresentation line in lines )
		{
			line.Highlight( false, null );
		}
	}

	public void Highlight( long lineIndex, bool enabled, bool[] wins )
	{
		lines[(int)lineIndex].Highlight( enabled, wins );
	}
}
