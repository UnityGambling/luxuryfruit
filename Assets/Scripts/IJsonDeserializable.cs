﻿

public interface IJsonDeserializable
{
    void PopulateFromJsonObject(JSONObject json);
}