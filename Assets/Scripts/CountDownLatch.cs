﻿using UnityEngine;
using System.Collections;
using System;

public sealed class CountDownLatch 
{
	public Action Synchronize;

	private uint remaining;

	public CountDownLatch( uint count )
	{
		remaining = count;
	}

	public void Signal()
	{
		remaining -= 1;

		if (remaining == 0 && Synchronize != null)
		{
			Synchronize();
		}
		else if (remaining < 0)
		{
			throw new InvalidOperationException("CountDownLatch received too many signals.");
		}
	}
}
