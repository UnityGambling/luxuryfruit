﻿using UnityEngine;
using System;
using LuxuryFruit.Library.Core.DTO;
using LuxuryFruit.Library.Client;

#if !(UNITY_WEBCANVAS || UNITY_WEBGL )

using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

public class TcpXmlClientCommProvider : MonoBehaviour , IClientCommProvider
{
	private XmlTcpClient client = null;
	private string connectAddress = null;
	private int connectPort = 0;
	
	public void Initialize(string address, int port)
	{
		connectAddress = address;
		connectPort = port;
		client = new XmlTcpClient();
		client.ConnectedToServer += () => { if (PlatformConnected != null)          { PlatformConnected(); } };
		client.DisconnectedFromServer += () => { if (PlatformDisconnected != null)  { PlatformDisconnected(); } };
		
		client.AddHandler("InitResponseDTO", (data) => { if (InitResponse != null)          { InitResponse(DeserializeXml<InitResponseDTO>(data)); } });
		client.AddHandler("SpinResponseDTO", (data) => { if (SpinResponse != null)          { SpinResponse(DeserializeXml<SpinResponseDTO>(data)); } });
		client.AddHandler("ResultResponseDTO", (data) => { if (ResultResponse != null)      { ResultResponse(DeserializeXml<ResultResponseDTO>(data)); } });
		client.AddHandler("CompleteResponseDTO", (data) => { if (CompleteResponse != null)  { CompleteResponse(DeserializeXml<CompleteResponseDTO>(data)); } });
		
		client.AddHandler("CreditBalanceDTO", (data) => { if (CreditBalancePush != null) { CreditBalancePush(DeserializeXml<CreditBalanceDTO>(data)); } });
	}
	
	private T DeserializeXml<T>(string data)
	{
		XmlSerializer serializer = new XmlSerializer(typeof(T));
		
		MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(data));
		T result = (T)serializer.Deserialize(ms);
		ms.Close();
		return result;
	}
	
	private string SerializeXml<T>(T obj)
	{
		MemoryStream ms = new MemoryStream();
		XmlWriter writer = XmlWriter.Create(ms, new XmlWriterSettings { Indent = false, NewLineHandling = NewLineHandling.None } );
		XmlSerializer serializer = new XmlSerializer(typeof(T));
		serializer.Serialize(writer, obj);
		return Encoding.UTF8.GetString( ms.GetBuffer() );
	}
	
	public void Startup()
	{
		client.Start(connectAddress, connectPort, this);
	}
	
	public void Shutdown()
	{
		client.Stop();
	}
	
	public event Action PlatformConnected;
	
	public event Action PlatformDisconnected;
	
	public void InitRequest(InitRequestDTO request)
	{
		client.Transmit(SerializeXml(request));
	}
	
	public void SpinRequest(SpinRequestDTO request)
	{
		client.Transmit(SerializeXml(request));
	}
	
	public void ResultRequest(ResultRequestDTO request)
	{
		client.Transmit(SerializeXml(request));
	}
	
	public void CompleteRequest(CompleteRequestDTO request)
	{
		client.Transmit(SerializeXml(request));
	}
	
	public event Action<InitResponseDTO> InitResponse;
	
	public event Action<SpinResponseDTO> SpinResponse;
	
	public event Action<ResultResponseDTO> ResultResponse;
	
	public event Action<CompleteResponseDTO> CompleteResponse;
	
	public event Action<CreditBalanceDTO> CreditBalancePush;
}

#else

public class TcpXmlClientCommProvider : MonoBehaviour, IClientCommProvider
{
    public void Initialize(string address, int port) { }

    public void Startup() { }
    public void Shutdown() { }

    public event Action PlatformConnected;
    public event Action PlatformDisconnected;

    public void InitRequest(InitRequestDTO request) { }
    public void SpinRequest(SpinRequestDTO request) { }
    public void ResultRequest(ResultRequestDTO request) { }
    public void CompleteRequest(CompleteRequestDTO request) { }

    public event Action<InitResponseDTO> InitResponse;
    public event Action<SpinResponseDTO> SpinResponse;
    public event Action<ResultResponseDTO> ResultResponse;
    public event Action<CompleteResponseDTO> CompleteResponse;
    public event Action<CreditBalanceDTO> CreditBalancePush;
}

#endif