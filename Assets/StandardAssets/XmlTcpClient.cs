﻿using System;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

public class XmlTcpClient
{
	public event Action ConnectedToServer;
	public event Action DisconnectedFromServer;
	
	private TcpClient client = null;
	private bool running = false;
	private string connectHost = null;
	private int connectPort = 0;

	private LinkedList<string> workList = new LinkedList<string>();
	private Regex extractNodeRegex = new Regex(@"\?><\s?(.*?)[\s>]");
	private Dictionary<string, Action<string>> nodeToHandler = new Dictionary<string, Action<string>>();
	
	public void Start(string host, int port, MonoBehaviour owner )
	{
		if (running)
		{
			throw new InvalidOperationException();
		}
		
		running = true;
		connectHost = host;
		connectPort = port;
		owner.StartCoroutine(MainRoutine());
	}
	
	public void Stop()
	{
		running = false;
		if (client != null)
		{
			client.Close();
		}
		client = null;
	}
	
	public void AddHandler(string node, Action<string> handler)
	{
		nodeToHandler.Add(node, handler);
	}
	
	public void Transmit(string line)
	{
		workList.AddLast(line);
	}
	
	private IEnumerator MainRoutine()
	{
		yield return new WaitForSeconds(0.1f);

		client = new TcpClient(connectHost, connectPort);
		Stream netStream = client.GetStream();
		StreamWriter writer = new StreamWriter(netStream);
		StreamReader reader = new StreamReader(writer.BaseStream);
		
		if (ConnectedToServer != null) { ConnectedToServer(); }
		
		while (running)
		{
			yield return null;
			// Check if this we have disconnected
			if (!client.Connected)
			{
				workList.Clear();
				if (DisconnectedFromServer != null) { DisconnectedFromServer(); }
				yield break;
			}
			
			// Check if there is data available
			if (client.Available > 0)
			{
				string buffer = reader.ReadLine();
				string nodeType = extractNodeRegex.Match(buffer).Groups[1].Value;
				
				if (nodeToHandler.ContainsKey(nodeType))
				{
					Action<string> handler = nodeToHandler[nodeType];
					if (handler != null)
					{
						handler(buffer);
					}
				}
			}
			
			// Check if we have data to send
			while (workList.Count > 0)
			{
				string work = workList.First.Value;
				workList.RemoveFirst();
				writer.WriteLine(work);
				writer.Flush();
			}
		}
		workList.Clear();
	}
	
}

