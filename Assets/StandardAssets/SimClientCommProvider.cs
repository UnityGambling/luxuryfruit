﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using LuxuryFruit.Library.Core.DTO;
using LuxuryFruit.Library.Client;
using LuxuryFruit.Library.Simulator;

// My FASTEST Ever Code and Magically it all works.
public class SimClientCommProvider : MonoBehaviour , IClientCommProvider
{		
	private static IMathProvider math = null;
	private static OutcomeRecordDTO p_outcome;
	private int p_id = 0;
	private double p_credit = 100000;
	
	public void Initialize(string address, int port)
	{
		Debug.Log("Local Simulation Client Started");
	}
	
	public void Startup()
	{
		//We're always "connected" more or less to a rest interface
		StartCoroutine(ClaimConnectedRoutine());
		math = new SimpleMathProvider();
		math.Startup();

		string json = "{\"strips\":[[10,0,7,1,0,6,9,2,3,0,8,1,0,5,0,1,0,4,0],[0,1,10,6,3,0,4,0,4,0,5,2,8,0,9,2,0,7,1],[9,7,0,10,2,0,8,1,5,0,6,0,0,4,2,3,1,3,2],[0,1,7,0,3,2,0,5,4,0,1,8,0,5,9,10,6,0,2],[8,0,1,2,0,7,0,10,0,4,6,0,9,5,0,1,0,3,1]],\"lines\":[[0,0,0,0,0],[1,1,1,1,1],[-1,-1,-1,-1,-1],[-1,-1,0,1,1],[1,1,0,-1,-1],[1,0,-1,0,1],[-1,0,1,0,-1],[1,-1,1,-1,1],[-1,1,-1,1,-1]],\"max_bet_per_line\":5.0,\"idle\":true,\"outcome\":{\"id\":0,\"credits\":100000.0,\"reel_stops\":[2,10,4,13,16],\"bet\":{\"num_lines\":9,\"bet_per_line\":5.0},\"wins\":[{\"line_index\":2,\"symbols\":[true,true,false,false,false],\"win\":500.0},{\"line_index\":3,\"symbols\":[true,true,false,false,false],\"win\":500.0},{\"line_index\":6,\"symbols\":[true,true,false,false,false],\"win\":500.0},{\"line_index\":8,\"symbols\":[true,true,false,false,false],\"win\":500.0}],\"status\":2}}";
		InitResponseDTO resp = new InitResponseDTO();
		resp.PopulateFromJsonObject(new JSONObject(json));
		p_outcome = resp.outcome;
	}

	private IEnumerator ClaimConnectedRoutine()
	{
		yield return new WaitForSeconds (1f);
		PlatformConnected ();
	}
	
	public void Shutdown()
	{
	}
	
	public event Action PlatformConnected;
	
	public event Action PlatformDisconnected;

    private void InitRequestRoutine( InitRequestDTO request )
    {
		InitResponseDTO response = new InitResponseDTO
		{
			idle = true,
			lines = math.GetPaylines(),
			strips = math.GetReelStrips(),
			max_bet_per_line = math.GetMaxBetPerLine(),
			outcome = p_outcome
		};
	    
        InitResponse(response);
    }

	public void InitRequest(InitRequestDTO request)
	{
		InitRequestRoutine(request);
	}

    private void SpinRequestRoutine(SpinRequestDTO request)
    {
		string json = "{\"id\":0,\"bet\":{\"num_lines\":9,\"bet_per_line\":5.0},\"credits\":100000.0,\"successful\":true}";
        SpinResponseDTO response = new SpinResponseDTO();
		response.PopulateFromJsonObject(new JSONObject(json));

		++p_id;
		p_outcome = math.GenerateOutcome(p_credit, request);
	    p_credit = p_outcome.credits;
	    response.bet.bet_per_line = request.bet.bet_per_line;
		response.bet.num_lines = request.bet.num_lines;
		response.id = p_id;
        SpinResponse(response);
    }
	
	public void SpinRequest(SpinRequestDTO request)
	{
		SpinRequestRoutine(request);
	}

    private void ResultRequestRoutine(ResultRequestDTO request)
    {
		string json = "{\"outcome\":{\"id\":0,\"credits\":368593.0,\"reel_stops\":[6,3,14,12,2],\"bet\":{\"num_lines\":9,\"bet_per_line\":5.0},\"wins\":[],\"status\":1}}";
		ResultResponseDTO response = new ResultResponseDTO();
        response.PopulateFromJsonObject(new JSONObject(json));

	    response.outcome = p_outcome;
        ResultResponse(response);
    }
	
	public void ResultRequest(ResultRequestDTO request)
	{
		ResultRequestRoutine(request);
	}

    private void CompleteRequestRoutine(CompleteRequestDTO request)
    {
		string json = "{\"id\":0,\"successful\":true}";
        CompleteResponseDTO response = new CompleteResponseDTO();
        response.PopulateFromJsonObject(new JSONObject(json));

	    response.id = p_id;
	    response.successful = true;
        CompleteResponse(response);
    }
	
	public void CompleteRequest(CompleteRequestDTO request)
	{
		CompleteRequestRoutine(request);
	}
	
	public event Action<InitResponseDTO> InitResponse;
	
	public event Action<SpinResponseDTO> SpinResponse;
	
	public event Action<ResultResponseDTO> ResultResponse;
	
	public event Action<CompleteResponseDTO> CompleteResponse;
	
	public event Action<CreditBalanceDTO> CreditBalancePush;
}
