﻿using UnityEngine;
using System.Collections;
using LuxuryFruit.Library.Client;
using LuxuryFruit.Library.Core.DTO;

public class PlatformPushReceiver : MonoBehaviour 
{
	private IClientCommProvider comm = null;

	private void OnEnable()
	{
		comm = (IClientCommProvider)gameObject.GetComponent(typeof(IClientCommProvider));
		comm.CreditBalancePush += OnCreditBalancePush;
	}

	private void OnCreditBalancePush( CreditBalanceDTO push )
	{
		Domain.Game.Credits = push.credits;
	}

	private void OnDisable()
	{
		if ( comm != null )
		{
			comm.CreditBalancePush -= OnCreditBalancePush;
		}
	}
}
