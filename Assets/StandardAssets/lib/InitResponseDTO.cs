﻿using System;

namespace LuxuryFruit.Library.Core.DTO
{
    /// <summary>
    /// Transfers the response to an initialization request
    /// </summary>
    [Serializable]
    public struct InitResponseDTO : IJsonDeserializable
    {
        /// <summary>
        /// First index addresses a line, second index returns a symbol identifier
        /// </summary>
        public long[][] strips;
        /// <summary>
        /// First index addresses a line, second index is a +/- offset from center (where 0 would be exactly on the reel stop)
        /// </summary>
        public long[][] lines;
        /// <summary>
        /// The max amount of money that can be bet for each line
        /// </summary>
        public double max_bet_per_line;
        /// <summary>
        /// If true, we're ready to accept new bets
        /// </summary>
        public bool idle;
        /// <summary>
        /// The most recent outcome, to initialize the client presentation. May be started, viewed, or completed.
        /// </summary>
        public OutcomeRecordDTO outcome;

        private long[][] LongJaggedArray2DFromJson( JSONObject jobj )
        {
            var outerJArray = jobj.list;
            long[][] outer = new long[outerJArray.Count][];
            for (int i=0; i < outerJArray.Count; ++i)
            {
                var innerJArray = outerJArray[i].list;
                long[] inner = new long[innerJArray.Count];

                for (int j = 0; j < innerJArray.Count; ++j )
                {
                    inner[j] = (long)innerJArray[j].n;
                }
                outer[i] = inner; 
            }

            return outer;
        }

        public void PopulateFromJsonObject(JSONObject json)
        {
            strips = LongJaggedArray2DFromJson(json.GetField("strips"));
            lines = LongJaggedArray2DFromJson(json.GetField("lines"));
            max_bet_per_line = json.GetDoubleField("max_bet_per_line");
            idle = json.GetBoolField("idle");
            outcome.PopulateFromJsonObject(json.GetField("outcome"));

            UnityEngine.Debug.Log("Made it through InitResponseDTO.PopulateFromJsonObject. max_bet_per_line = " + max_bet_per_line);
        }
    }
}
