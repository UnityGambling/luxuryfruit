﻿using System;

namespace LuxuryFruit.Library.Core.DTO
{
    /// <summary>
    /// Transfers a credit balance to the client when pushed by the Platform
    /// </summary>
    [Serializable]
    public struct CreditBalanceDTO : IJsonDeserializable
    {
        /// <summary>
        /// The current credit balance
        /// </summary>
        public double credits;

        public void PopulateFromJsonObject(JSONObject json)
        {
            credits = json.GetDoubleField("credits");
        }
    }
}
