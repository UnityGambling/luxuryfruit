﻿using System;

namespace LuxuryFruit.Library.Core.DTO
{
    /// <summary>
    /// Transfers a request for a spin result
    /// </summary>
    [Serializable]
    public struct ResultRequestDTO : IJsonSerializable
    {
        /// <summary>
        /// The persistence id for the outcome 
        /// </summary>
        public long id;

        public JSONObject ToJsonObject()
        {
            JSONObject json = new JSONObject( JSONObject.Type.OBJECT );
            
            json.AddField("id", id);
            
            return json;
        }
    }
}
