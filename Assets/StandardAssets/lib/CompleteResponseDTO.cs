﻿using System;

namespace LuxuryFruit.Library.Core.DTO
{
    /// <summary>
    /// Transfers a response to a complete request
    /// </summary>
    [Serializable]
    public struct CompleteResponseDTO : IJsonDeserializable
    {
        /// <summary>
        /// The persistence id that was requested to complete
        /// </summary>
        public long id;
        /// <summary>
        /// If true, the last spin has been successfully completed
        /// </summary>
        public bool successful;

        public void PopulateFromJsonObject(JSONObject json)
        {
            id = json.GetLongField("id");
            successful = json.GetBoolField("successful");
        }
    }
}
