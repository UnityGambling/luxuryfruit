﻿using LuxuryFruit.Library.Core.DTO;
using System;

namespace LuxuryFruit.Library.Client
{
    /// <summary>
    /// Abstraction for the communication / serialization system used by the Platform and Client
    /// </summary>
    public interface IClientCommProvider
    {
        /// <summary>
        /// Initialize the underlying communication structures
        /// </summary>
        void Startup();
        /// <summary>
        /// Shutdown the underlying communication structures
        /// </summary>
        void Shutdown();

        /// <summary>
        /// Invoked when successfully connected to the Platform
        /// </summary>
        event Action PlatformConnected;
        /// <summary>
        /// Invoked when disconnected from the Platform
        /// </summary>
        event Action PlatformDisconnected;

        /// <summary>
        /// Call to send an InitRequest to the Platform
        /// </summary>
        /// <param name="request">The payload</param>
        void InitRequest(InitRequestDTO request);
        /// <summary>
        /// Call to send a SpinRequest to the Platform
        /// </summary>
        /// <param name="request">The payload</param>
        void SpinRequest(SpinRequestDTO request);
        /// <summary>
        /// Call to send a ResultRequest to the Platform
        /// </summary>
        /// <param name="request">The payload</param>
        void ResultRequest(ResultRequestDTO request);
        /// <summary>
        /// Call to send a CompleteRequest to the Platform
        /// </summary>
        /// <param name="request">The payload</param>
        void CompleteRequest(CompleteRequestDTO request);

        /// <summary>
        /// Invoked when an InitResponse is received from the Platform
        /// </summary>
        event Action<InitResponseDTO> InitResponse;
        /// <summary>
        /// Invoked when a SpinResponse is received from the Platform
        /// </summary>
        event Action<SpinResponseDTO> SpinResponse;
        /// <summary>
        /// Invoked when a ResultResponse is received from the Platform
        /// </summary>
        event Action<ResultResponseDTO> ResultResponse;
        /// <summary>
        /// Invoked when a CompleteResponse is received from the Platform
        /// </summary>
        event Action<CompleteResponseDTO> CompleteResponse;
        /// <summary>
        /// Invoked when a CreditBalance push is reveived from the Platform
        /// </summary>
        event Action<CreditBalanceDTO> CreditBalancePush;
    }
}
