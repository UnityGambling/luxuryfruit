﻿using System;

namespace LuxuryFruit.Library.Core.DTO
{
    /// <summary>
    /// Transfers the response to a result request
    /// </summary>
    [Serializable]
    public struct ResultResponseDTO : IJsonDeserializable
    {
        /// <summary>
        /// The outcome information that was requested
        /// </summary>
        public OutcomeRecordDTO outcome;

        public void PopulateFromJsonObject(JSONObject json)
        {
            outcome.PopulateFromJsonObject( json.GetField("outcome") );
        }
    }
}
