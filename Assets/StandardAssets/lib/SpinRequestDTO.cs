﻿using System;

namespace LuxuryFruit.Library.Core.DTO
{
    /// <summary>
    /// Transfers a single spin request
    /// </summary>
    [Serializable]
    public struct SpinRequestDTO : IJsonSerializable
    {
        /// <summary>
        /// The bet information
        /// </summary>
        public BetConfigDTO bet;

        public JSONObject ToJsonObject()
        {
            JSONObject json = new JSONObject(JSONObject.Type.OBJECT);

            json.AddField("bet", bet.ToJsonObject());

            return json;
        }
    }
}
