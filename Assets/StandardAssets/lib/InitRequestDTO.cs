﻿using System;

namespace LuxuryFruit.Library.Core.DTO
{
    /// <summary>
    /// Transfers an initialization request
    /// </summary>
    [Serializable]
    public struct InitRequestDTO : IJsonSerializable
    {
        public JSONObject ToJsonObject()
        {
            return new JSONObject( JSONObject.Type.OBJECT );
        }
    }
}
