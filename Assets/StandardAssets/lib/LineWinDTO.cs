﻿using System;

namespace LuxuryFruit.Library.Core.DTO
{
    /// <summary>
    /// Transfers information about a single winning line in OutcomeRecord.wins
    /// </summary>
    [Serializable]
    public struct LineWinDTO : IJsonDeserializable
    {
        /// <summary>
        /// The index of the line, maps to InitResponseDTO.lines
        /// </summary>
        public long line_index;

        /// <summary>
        /// Indices here map to reel numbers, and true/false represents if the symbol under the line on that reel contributed to the win
        /// </summary>
        public bool[] symbols;

        /// <summary>
        /// The amount of money won from this line
        /// </summary>
        public double win;

        public void PopulateFromJsonObject(JSONObject json)
        {
            line_index = json.GetLongField("line_index");
            
            var symbolJArray = json.GetField("symbols");
            symbols = new bool[symbolJArray.Count];
            for (int i = 0; i < symbolJArray.Count; ++i )
            {
                symbols[i] = symbolJArray[i].b;
            }

            win = json.GetDoubleField("win");
        }
    }
}
