﻿using System;

namespace LuxuryFruit.Library.Core.DTO
{
    /// <summary>
    /// Transfers the result of a bet/spin. May appear in an InitResponse or a ResultResponse
    /// </summary>
    [Serializable]
    public struct OutcomeRecordDTO : IJsonDeserializable
    {
        /// <summary>
        /// The persistence id of this outcome
        /// </summary>
        public long id;
        /// <summary>
        /// The credit balance as a result of this outcome
        /// </summary>
        public double credits;
        /// <summary>
        /// An array of indices for the reel stops. Indices map to InitResponseDTO.strips
        /// </summary>
        public long[] reel_stops;
        /// <summary>
        /// The bet that initiated this spin and outcome
        /// </summary>
        public BetConfigDTO bet;
        /// <summary>
        /// The list of winning lines 
        /// </summary>
        public LineWinDTO[] wins;
        /// <summary>
        /// The status of this outcome -whether it has been started, viewed, or flagged as complete
        /// </summary>
        public OutcomeStatus status;

        public void PopulateFromJsonObject(JSONObject json)
        {
            id = json.GetLongField("id");
            credits = json.GetDoubleField("credits");

            var reelStopsJArray = json.GetField("reel_stops").list;
            reel_stops = new long[reelStopsJArray.Count];
            for (int i = 0; i < reelStopsJArray.Count; ++i)
            {
                reel_stops[i] = (long)reelStopsJArray[i].n;
            }

            bet.PopulateFromJsonObject(json.GetField("bet"));

            var winsJArray = json.GetField("wins").list;
            wins = new LineWinDTO[winsJArray.Count];
            for (int i = 0; i < winsJArray.Count; ++i )
            {
                wins[i].PopulateFromJsonObject( winsJArray[i] );
            }

            string statusString = null;
            statusString = json.GetStringField("status");
            switch (statusString)
            {
                case "Started":     { status = OutcomeStatus.Started; break; }
                case "Viewed":      { status = OutcomeStatus.Viewed;  break; }
                case "Completed":   { status = OutcomeStatus.Completed; break; }
            }
        }
    }
}
