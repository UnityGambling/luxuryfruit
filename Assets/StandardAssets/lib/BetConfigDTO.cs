﻿using System;

namespace LuxuryFruit.Library.Core.DTO
{
    /// <summary>
    /// Transfers information about a bet
    /// </summary>
    [Serializable]
    public struct BetConfigDTO : IJsonDeserializable, IJsonSerializable
    {
        /// <summary>
        /// The number of lines activated
        /// </summary>
        public long num_lines;

        /// <summary>
        /// The amount of money bet per line
        /// </summary>
        public double bet_per_line;

        public void PopulateFromJsonObject(JSONObject json)
        {
            num_lines = json.GetLongField( "num_lines" );
            bet_per_line = json.GetDoubleField( "bet_per_line" );
        }

        public JSONObject ToJsonObject()
        {
            JSONObject json = new JSONObject( JSONObject.Type.OBJECT );

            json.AddField( "num_lines", num_lines) ;
            json.AddField( "bet_per_line", bet_per_line );

            return json;
        }
    }
}
