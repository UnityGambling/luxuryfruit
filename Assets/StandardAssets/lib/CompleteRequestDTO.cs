﻿using System;

namespace LuxuryFruit.Library.Core.DTO
{
    /// <summary>
    /// Transfers a request to complete a spin outcome record
    /// </summary>
    [Serializable]
    public struct CompleteRequestDTO : IJsonSerializable
    {
        /// <summary>
        /// The persistence id that we're requesting to complete
        /// </summary>
        public long id;

        public JSONObject ToJsonObject()
        {
            JSONObject json = new JSONObject( JSONObject.Type.OBJECT );
            json.AddField( "id", id );
            return json;
        }
    }
}
