﻿using System;

namespace LuxuryFruit.Library.Core
{
    /// <summary>
    /// Represents the state of an outcome record (in the persistence layer)
    /// </summary>
    [Serializable]
    public enum OutcomeStatus
    {
        /// <summary>
        /// The spin has been started, but the results have not been viewed or marked as completed
        /// </summary>
        Started,
        /// <summary>
        /// The spin has been started and has been viewed at least once, but has not been marked as completed
        /// </summary>
        Viewed,
        /// <summary>
        /// The spin has been started, viewed, and marked as completed
        /// </summary>
        Completed
    }
}
