﻿using System;

namespace LuxuryFruit.Library.Core.DTO
{
    /// <summary>
    /// Transfers the response to a spin request
    /// </summary>
    [Serializable]
    public struct SpinResponseDTO : IJsonDeserializable
    {
        /// <summary>
        /// The persistence ID for the outcome
        /// </summary>
        public long id;
        /// <summary>
        /// The bet that was used to initiate this spin
        /// </summary>
        public BetConfigDTO bet;
        /// <summary>
        /// The credit balance after this spin
        /// </summary>
        public double credits;
        /// <summary>
        /// If true, the bet/spin request was successful
        /// </summary>
        public bool successful;

        public void PopulateFromJsonObject(JSONObject json)
        {
            id = json.GetLongField("id");
            bet.PopulateFromJsonObject( json.GetField("bet") );
            credits = json.GetDoubleField("credits");
            successful = json.GetBoolField("successful");
        }
    }
}
