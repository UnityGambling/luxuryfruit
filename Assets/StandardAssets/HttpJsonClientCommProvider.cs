﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using LuxuryFruit.Library.Core.DTO;
using LuxuryFruit.Library.Client;

public class HttpJsonClientCommProvider : MonoBehaviour , IClientCommProvider
{
	private string connectAddress = null;
	private int connectPort = 0;
	
	public void Initialize(string address, int port)
	{
		if (!address.StartsWith("http://"))
		{
			address = "http://" + address;
		}

		connectAddress = address;
		connectPort = port;
	}
	
	public void Startup()
	{
		//We're always "connected" more or less to a rest interface
		StartCoroutine(ClaimConnectedRoutine());
	}

	private IEnumerator ClaimConnectedRoutine()
	{
		yield return new WaitForSeconds (1f);
		PlatformConnected ();
	}
	
	public void Shutdown()
	{
	}
	
	public event Action PlatformConnected;
	
	public event Action PlatformDisconnected;

    /*
	private IEnumerator RequestRoutine<TRequest, TResponse>( string endpoint, TRequest request, Action<TResponse> responseHandler ) 
        where TRequest : IJsonSerializable
        where TResponse : IJsonDeserializable
	{
        string json = request.ToJsonObject().ToString();
		Dictionary<string,string> headers = new Dictionary<string,string>();
		headers.Add( "Content-Type", "application/json; charset=utf-8" );
        headers.Add( "Origin", "http://localhost:8080" );

		Debug.Log (string.Format("Hitting {0} with {1}", string.Format( "{0}:{1}/LuxuryFruit/{2}", connectAddress, connectPort, endpoint ), json ));

		WWW wwwRequest = new WWW( string.Format( "{0}:{1}/LuxuryFruit/{2}", connectAddress, connectPort, endpoint ), System.Text.Encoding.UTF8.GetBytes( json ), headers );
		yield return wwwRequest;

        TResponse response = default(TResponse);
        JSONObject jo = new JSONObject(wwwRequest.text);
          
        response.PopulateFromJsonObject( jo );

		responseHandler( response );
	}
     */

    private IEnumerator InitRequestRoutine( InitRequestDTO request )
    {
        string json = request.ToJsonObject().ToString();
        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json; charset=utf-8");
        headers.Add("Origin", "http://localhost:8080");

        WWW wwwRequest = new WWW(string.Format("{0}:{1}/LuxuryFruit/InitRequest", connectAddress, connectPort ), System.Text.Encoding.UTF8.GetBytes(json), headers);
        yield return wwwRequest;

        InitResponseDTO response = new InitResponseDTO();
        response.PopulateFromJsonObject(new JSONObject(wwwRequest.text));

        InitResponse(response);
    }

	public void InitRequest(InitRequestDTO request)
	{
        StartCoroutine(InitRequestRoutine(request));
	}

    private IEnumerator SpinRequestRoutine(SpinRequestDTO request)
    {
        string json = request.ToJsonObject().ToString();
        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json; charset=utf-8");
        headers.Add("Origin", "http://localhost:8080");

        WWW wwwRequest = new WWW(string.Format("{0}:{1}/LuxuryFruit/SpinRequest", connectAddress, connectPort), System.Text.Encoding.UTF8.GetBytes(json), headers);
        yield return wwwRequest;

        SpinResponseDTO response = new SpinResponseDTO();
        response.PopulateFromJsonObject(new JSONObject(wwwRequest.text));

        SpinResponse(response);
    }
	
	public void SpinRequest(SpinRequestDTO request)
	{
		StartCoroutine(SpinRequestRoutine(request));
	}

    private IEnumerator ResultRequestRoutine(ResultRequestDTO request)
    {
		JSONObject job = request.ToJsonObject();
		string json = job.ToString();
		Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json; charset=utf-8");
        headers.Add("Origin", "http://localhost:8080");

        WWW wwwRequest = new WWW(string.Format("{0}:{1}/LuxuryFruit/ResultRequest", connectAddress, connectPort), System.Text.Encoding.UTF8.GetBytes(json), headers);
        yield return wwwRequest;

        ResultResponseDTO response = new ResultResponseDTO();
        response.PopulateFromJsonObject(new JSONObject(wwwRequest.text));

        ResultResponse(response);
    }
	
	public void ResultRequest(ResultRequestDTO request)
	{
		StartCoroutine(ResultRequestRoutine(request));
	}

    private IEnumerator CompleteRequestRoutine(CompleteRequestDTO request)
    {
        string json = request.ToJsonObject().ToString();
        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json; charset=utf-8");
        headers.Add("Origin", "http://localhost:8080");

        WWW wwwRequest = new WWW(string.Format("{0}:{1}/LuxuryFruit/CompleteRequest", connectAddress, connectPort), System.Text.Encoding.UTF8.GetBytes(json), headers);
        yield return wwwRequest;

        CompleteResponseDTO response = new CompleteResponseDTO();
        response.PopulateFromJsonObject(new JSONObject(wwwRequest.text));

        CompleteResponse(response);
    }
	
	public void CompleteRequest(CompleteRequestDTO request)
	{
		StartCoroutine(CompleteRequestRoutine(request));
	}
	
	public event Action<InitResponseDTO> InitResponse;
	
	public event Action<SpinResponseDTO> SpinResponse;
	
	public event Action<ResultResponseDTO> ResultResponse;
	
	public event Action<CompleteResponseDTO> CompleteResponse;
	
	public event Action<CreditBalanceDTO> CreditBalancePush;
}
