﻿using System;
using UnityEngine;
using System.Diagnostics;
using System.IO;
using Debug=UnityEngine.Debug;

public class LocalPlatformManager : MonoBehaviour 
{

#if UNITY_STANDALONE
	private Process proc = new Process();

	private void OnEnable()
	{
		proc.StartInfo.FileName = Path.Combine( Application.dataPath, "../LocalPlatform/Platform.exe");
		proc.StartInfo.Arguments = string.Format("{0} {1}",Domain.Platform.PlatformCommType == CommType.TCP ? "tcp" : "http",Domain.Platform.LocalPort.ToString());
		proc.StartInfo.CreateNoWindow = true; 
		proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
		proc.StartInfo.UseShellExecute = false;
		proc.StartInfo.RedirectStandardInput = true;
		proc.Start();
		proc.StandardInput.WriteLine(string.Empty); //Prime the Standard Input stream.
	}

#if UNITY_EDITOR
	private const int UIWidth = 100;
	private const int UIHeight = 30;

	private void OnGUI()
	{
		GUILayout.BeginArea(new Rect((Screen.width-UIWidth), 0, UIWidth, UIHeight), GUI.skin.GetStyle("box"));
		if( GUILayout.Button("Add Credits") )
		{
			StreamWriter input = proc.StandardInput;
			input.WriteLine("credits");
			input.Flush();
		}
		GUILayout.EndArea();
	}
#endif

	private void OnDisable()
	{
		proc.CloseMainWindow();
		proc.Close();
	}
#else
	private void OnEnable()
	{
		Debug.LogError("Local Platform can only be started in PC/OSX/Linux platform");
	}
#endif
}
